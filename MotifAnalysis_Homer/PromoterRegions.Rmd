---
title: "Define promoters"
output:
  html_document:
    toc: true
---

# Introduction

There are no strict rules about what range in or around a gene contains the promoter regions, but it is generally considered to be within the 1kb region immediately upstream of the transcription start site.

Here, we will make a bed file describing the promoter regions of genes that were considered DE in each of the comparisons in this experiment.

# DE Genes

```{r}
source("../src/ProjectWide.R")
source("../src/BedFiles.R")
FDR=getFDR()
promterRegionSize=1000
```

 * We are an FDR of `r FDR` to define "DE" genes. 
 * We are defining the promter region as the `r promterRegionSize`bp region immediately upstream of the gene.

Get the list of differenctially expressed genes from each of the comparisons.
```{r}
fnameBase="../DifferentialExpression/results/DifferentialExpression-"

#create empty list to hold DE results
comparisons=c("at2hours", "at4hours", "at8hours")
DElist = as.list(comparisons) # DElist for the list of DE genes.
names(DElist)=comparisons

for (comp in comparisons){
  d = read.delim(file=paste0(fnameBase, comp, ".txt.gz"), stringsAsFactors=F)
  d = d[d$fdr<=FDR,]
  DElist[[comp]] = d$gene
}

totalDE=lapply(DElist, length)
totalDE
```


# Arabidopsis reference data

```{r echo=FALSE}
# The fasta files are large, and really don't need to be version controled.
# So we will store them in a folder that git will ignore.
system("mkdir intermediateFiles") # in case the folder has not been made yet.
```

Read in the Tair 10 bed file.
```{r}
# Read in genes
T10 <- read.bed("../ExternalDataSets/TAIR10.bed.gz")
# only keep the columns we need
T10 = T10[,c(1:4,6,13)]
# Get a name for each gene rather than a name for each transcript.
T10$name = truncateTranscriptID(T10$name)

# the getPromoterRange function looks for unique identifiers in an "ID" column
T10$ID = T10$name

# Set up this data frame so that each gene is listed only once 
# and uses the most downstream chromosome start positions.
T10 = T10[order(T10$chromStart, decreasing = T),]
T10 = T10[!duplicated(T10$ID),]
```

To make fasta files, we will need the genome sequence.
```{r echo=F}
# the A_thaliana_Jun_2009.2bit can be downloaded from 
# http://igbquickload.org/A_thaliana_Jun_2009/
```
```{r}
suppressPackageStartupMessages(library(rtracklayer))
suppressPackageStartupMessages(library(Biostrings))
fname="../ExternalDataSets/A_thaliana_Jun_2009.2bit"
Genome <- import.2bit(con=fname)
```

# Promoter Regions

Write a bed file of promoter regions for DE genes in each set.
```{r}
writePromoterRegionsBed=function(T10, DEgenes, prom.fname="results/Promoters.bed"){
  # Get the promoter region for each gene.
  proms <- getPromoterRange(T10, genes=DEgenes, size=promterRegionSize)
  
  # indicate the gene strand for each promoter
  proms=merge(proms, T10[,c("ID", "strand")], by="ID")
  
  # put columns in bed format order and order rows by chromosome and stard/stop positions.
  proms$score=0
  proms = proms[order(proms$chromosome, proms$promStart, proms$promEnd),c(2,3,4,1,6,5)]
  
  # cut off any that go past the end of the chromosome
  chromLimits = read.table("../ExternalDataSets/genome.txt")
  limits = chromLimits$V2
  names(limits) = chromLimits$V1
  proms = trimToChromEnds(proms, limits=limits)
  
  # Write this to a file. Zip the file.
  write.bed(proms, prom.fname)
  system(paste('gzip -f', prom.fname))
  print(paste0("Created file: ", prom.fname, "."))
  
  # the next function requires standard bed file names
  names(proms)=c("chromosome", "chromStart", "chromEnd", "name", "score", "strand")
  return(proms)
}
```

Write a fasta file for the promoter regions for DE genes in each set.
```{r}
writePromoterRegionsFasta = function(regions, Genome, fa.fname="results/promters.fasta"){
  seqs = subseq(Genome[regions$chromosome], #Seqs being short for sequences
                start=regions$chromStart+1, 
                end=regions$chromEnd) 
  names(seqs)=regions$name
  # pick out the genes that were on the negative strand and flip the sequences
  neg=regions$name[regions$strand=="-"]
  seqs[neg] = reverseComplement(seqs[neg])
  # write fasta file
  writeXStringSet(seqs, fa.fname, format="fasta")
  print(paste0("Created file: ", fa.fname, ", with ", length(seqs), " sequences."))
}
```

Use the functions in the last two chunks to write a bed file and a fasta file for the DE genes in each of the comparisons.
```{r}
for (comp in comparisons){
  proms=writePromoterRegionsBed(T10, 
                     DEgenes=DElist[[comp]],
                     prom.fname=paste0("intermediateFiles/Promoters-", comp, ".bed"))
  writePromoterRegionsFasta(regions=proms, Genome=Genome, 
                          fa.fname=paste0("intermediateFiles/Promoters-", comp, ".fasta"))
}
```

# Background

Select a set of promoters to use as a backround in looking for motifs.  Select genes that were expressed in this experiment, but did not change acorss samples.

```{r}
#get 2 hour data
back=read.delim(file=paste0(fnameBase, "at2hours.txt.gz"), stringsAsFactors=F)
back=back[back$C2.ave>.5 & back$C2.ave<80, ] #keep only moderate expression genes
back=back[,c("gene", "fdr")]

#get 4 hour data
tmp=read.delim(file=paste0(fnameBase, "at4hours.txt.gz"), stringsAsFactors=F)
tmp=tmp[tmp$C4.ave>.5 & tmp$C4.ave<80, ] #keep only moderate expression genes
tmp=tmp[,c("gene", "fdr")]

back=merge(back, tmp, by="gene")

#get 8 hour data
tmp=read.delim(file=paste0(fnameBase, "at8hours.txt.gz"), stringsAsFactors=F)
tmp=tmp[tmp$C8.ave>.5 & tmp$C8.ave<80, ] #keep only moderate expression genes
tmp=tmp[,c("gene", "fdr")]

back=merge(back, tmp, by="gene")
names(back)=c("gene", "at2hours", "at4hours", "at8hours")

# pick out stable genes
stable=apply(back[2:4], 1, function(x){!any(x<.9)})
stableBackground = back$gene[stable]
```

Create fasta file for background.
```{r}
  proms=writePromoterRegionsBed(T10, 
                     DEgenes=stableBackground,
                     prom.fname=paste0("intermediateFiles/", "Background", ".bed"))
  writePromoterRegionsFasta(regions=proms, Genome=Genome, 
                          fa.fname=paste0("intermediateFiles/", "Background", ".fasta"))
```



# Conclusions

 * The bed file(s) can be used to visualize the promoters in IGB. 
 * The fasta file(s) can be used to look for motifs.

# Technical Details
Session Info
```{r}
sessionInfo()
```

Functions defined outside of this document.
```{r}
read.bed
getPromoterRange
trimToChromEnds
write.bed
```
