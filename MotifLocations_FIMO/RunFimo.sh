#!/bin/bash

#addMeme # make sure the meme suite in on your path
# run this using ./RunFimo.sh &> RunFimo.log

# a folder to store resources created for FIMO
f=FIMO_resources
mkdir $f

#### This section only needs to be run once

# a folder to hold intermediate file - files I might want to look at but I don't need to add to the repo
tmp=intermediateFiles
mkdir $tmp

# Make a background file based on the entire genome
#fasta-get-markov -m 4 ../ExternalDatasets/A_thaliana_Jun_2009.fa > $f/MarkovBackground-A_thaliana_Jun_2009-wholeGenome.txt

# Make a background file based on the sequences that are annoted as being in genes and/or promoters.
#addBedtools # make sure bedtools is on your path
gunzip -c ../ExternalDatasets/TAIR10.bed.gz | cut -f 1-4 | cat - ../ExternalDatasets/GeneStructures/promoter-1kb.bed | sort -k 1,1 -k2,2n | cut -f 1-3 | bedtools merge > $tmp/geneAndPromoterBackground.bed
bedtools getfasta -fi ../ExternalDatasets/A_thaliana_Jun_2009.fa -bed $tmp/geneAndPromoterBackground.bed > $tmp/geneAndPromoterBackground.fasta
fasta-get-markov -m 4 $tmp/geneAndPromoterBackground.fasta > $f/MarkovBackground-A_thaliana_Jun_2009-geneAndPromoterBackground.txt

# Make a background file based on the sequences that are annoted as being in genes and/or promoters and/or downstream regions.
gunzip -c ../ExternalDatasets/TAIR10.bed.gz | cut -f 1-4 | cat - ../ExternalDatasets/GeneStructures/promoter-1kb.bed ../ExternalDatasets/GeneStructures/downstream-1kb.bed | sort -k 1,1 -k2,2n | cut -f 1-3 | bedtools merge > $tmp/geneAndFlanking.bed
bedtools getfasta -fi ../ExternalDatasets/A_thaliana_Jun_2009.fa -bed $tmp/geneAndFlanking.bed > $tmp/geneAndFlanking.fasta
fasta-get-markov -m 4 $tmp/geneAndPromoterBackground.fasta > $f/MarkovBackground-A_thaliana_Jun_2009-geneAndFlanking.txt


#### This section is where where FIMO is actualling doing its thing - finding individual motif occurrences.

mkdir results

SEQS=../ExternalDatasets/A_thaliana_Jun_2009.fa
BGFILE=$f/MarkovBackground-A_thaliana_Jun_2009-geneAndPromoterBackground.txt

PLT3motif=data_motifs/DAP-seq_motif/PLT3_col_a/meme_out/meme_m1.txt
fimo --bgfile $BGFILE --oc results/fimo_out_DAP-seq-PLT3 $PLT3motif $SEQS

motif=data_motifs/JASPAR_MA0571.1/MA0571.1.pfm.memeFormat.txt
#fimo --bgfile $BGFILE --thresh 1e-3 --no-qvalue --oc results/fimo_out_MA0571.1_p1e-3 $motif $SEQS
fimo --bgfile $BGFILE --max-stored-scores 1000000 --thresh 1e-3 --no-qvalue --oc results/fimo_out_MA0571.1_p1e-3 $motif $SEQS
# if you get an error about dropping scores to reclaim memory, you can increase the number of --max-stored-scores
# but I don't know how far that option can be pushed.

fimo --bgfile $BGFILE --max-stored-scores 1000000 --thresh 1e-4 --no-qvalue --oc results/fimo_out_MA0571.1_p1e-4 $motif $SEQS 



#### specifiy pvalue threshold, run fimo with several different values.
#### ---doing this showed that memory constraints were limiting the results if additional memory was not given.

#fimo --bgfile $BGFILE --thresh 1e-10 --no-qvalue --oc fimo_out_MA0571.1_p1e-10 $motif $SEQS

#fimo --bgfile $BGFILE --thresh 1e-8 --no-qvalue --oc fimo_out_MA0571.1_p1e-8 $motif $SEQS

#fimo --bgfile $BGFILE --thresh 1e-6 --no-qvalue --oc fimo_out_MA0571.1_p1e-6 $motif $SEQS

#

#fimo --bgfile $BGFILE --thresh 1e-3 --no-qvalue --oc fimo_out_MA0571.1_p1e-3 $motif $SEQS

#fimo --bgfile $BGFILE --thresh 1e-2 --no-qvalue --oc fimo_out_MA0571.1_p1e-2 $motif $SEQS

#fimo --bgfile $BGFILE --thresh 1e-1 --no-qvalue --oc fimo_out_MA0571.1_p1e-1 $motif $SEQS

#fimo --bgfile $BGFILE --thresh 1 --no-qvalue --oc fimo_out_MA0571.1_p1 $motif $SEQS

