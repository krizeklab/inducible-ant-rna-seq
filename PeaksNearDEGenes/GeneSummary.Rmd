---
title: "Gene Summary"
output:
  html_document: 
    toc: true
---

```{r setup, echo=FALSE}
library(knitr)
knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",", digits=2)
})
knitr::read_chunk("chunks.R")

peak="motif occurence" #use the same code for talking about peaks and motif occurences.
```


Make a single reference file that contains both the differential expression results, and the motif occurence data.

# Full list

This file will include every gene. So start with the gene annotations.
```{r}
source("../src/ProjectWide.R")
annots = getAnnots()
```

Merge in the differential expression data.
```{r}
de = read.delim("../DifferentialExpression/results/DifferentialExpression-all.txt.gz")
# from this, we only need the fdr adn logFC columns
keep = grep("gene|logFC|fdr", names(de))

res = merge(annots, de[,keep], by="gene", all.x=T)
```

Merge in the motif occurrence data for MA0571.1.
Recall from MA0571.1.Rmd that MA0571.1_FIMO_all_CountPerGeneRegion.txt summarizes the number of occurrences of the motife within each region.
Recall from the interpretation of the output of TestAll_MA0571.1.Rmd (see interpretation folder) that the promoter is the most likley region to influence expression.
```{r}
mo = read.delim("results/MA0571.1_FIMO_all_CountPerGeneRegion.txt", comment.char = "#")
# only use the promoter region, and use a descriptive column name.
inPromo = data.frame(gene=mo$gene, numOccurrencesOfMA0571.1inPromoter=mo$promoter)

res = merge(res, inPromo, by="gene", all.x=T)
```

The CountPerGeneRegion file only includes genes that were part of the table of genes-near-motif occurrences.  If the gene had one in some other region but not in the promoter, then the gene is listed as having 0 in the promoter.  For genes that did not have any motif occurrences, the merged table will say "NA".  We can safely replace these NA values with 0's to avoid any confusion.
```{r}
naVals = which(is.na(res$numOccurrencesOfMA0571.1inPromoter))
res$numOccurrencesOfMA0571.1inPromoter[naVals] = 0
```


The description field is often long, so make it the last column in the table.
```{r}
# The table should include all columns that are not the description column, 
# followed by the one that is description column.
newCols = c(grep("desc", names(res), invert=T), 
						grep("desc", names(res)))
res = res[,newCols]
```

Save this to a file with an informative name.
```{r}
write.table(res, "results/GeneSummary_ExpressionChangeAndMA0571.1.txt", 
						col.names = T, row.names = F, quote = F, sep = "\t")
```

# Short list

Make a smaller table that has only the likely targets.
Keep only genes that:

 * were DE based on the GLM test (the test that incorporates all samples and accounts for time point and flat) and  
 * have a motif occurrence in their promoter region.
```{r}
source("../src/ProjectWide.R")
FDR = getFDR()

res2 = res[which(res$numOccurrencesOfMA0571.1inPromoter>0),]
res2 = res2[which(res2$fdr.GLM<FDR),]
#res2 = res2[which(res2$fdr.at2hours<FDR | res2$fdr.GLM<FDR ),]
#res2 = res2[which(res2$fdr.GLM<FDR & res2$fdr.GLM<FDR),]
```
Of these `r nrow(res2)` genes: 

 * `r sum(res2$fdr.at2hours < FDR)` are DE in the 2-hour time point.
 * `r sum(res2$fdr.at4hours < FDR)` are DE in the 4-hour time point.
 * `r sum(res2$fdr.at8hours < FDR)` are DE in the 8-hour time point.
 * `r sum( res2$fdr.at2hours < FDR | res2$fdr.at4hours < FDR | res2$fdr.at8hours < FDR)` are DE in any one individual time point.


These `r nrow(res2)` genes represent the genes most likley to be direct targets of ANT because there is evidence of a binding site (MA0571.1 occurrenece, see MA0571.1.Rmd for min score) and evidence that ANT causes a change in the expression level (FDR of `r FDR`).  

Put these in order by fold change using the GLM test, then put the `r sum(res2$fdr.at2hours<FDR)` that were DE at 2 hours at the top.
```{r}
res2 = res2[order(res2$logFC.GLM, decreasing = T), ]
res2 = res2[order(res2$fdr.at2hours < FDR, decreasing = T), ]
```


Save this to a file with an informative name.
```{r}
write.table(res2, "results/GenesOfInterest_ExpressionChangeAndMA0571.1.txt", 
						col.names = T, row.names = F, quote = F, sep = "\t")
```


