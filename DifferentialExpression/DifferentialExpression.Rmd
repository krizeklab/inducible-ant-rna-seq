---
title: "Differentially Expressed Genes"
output:
  html_document:
    toc: true
---

# Introduction

In this study, Arabidopsis thaliana plants with an added 35S:ANT-GR gene were used. The activity of the added ANT is dependent on dex.

The goal of this analysis is to identify which genes, if any, are differentially expressed between the dex-treated and mock-treated samples. 

**Questions**

 * Which genes are differentially expressed in response to dex treatment at 2 hours? at 4 hours? or at 8 hours? 
 * Which genes are differentially expressed at multiple time points?

# Getting Started

## Load libraries and set variables

```{r}
# biocLite("edgeR")
suppressPackageStartupMessages(library(edgeR))
suppressPackageStartupMessages(library(knitr))
source("../src/ProjectWide.R")
source("../src/DrawPlots.R")
source("src/DetermineDE.R")
FDR=getFDR() #defined in ProjectWide.R, see Technical Details section
```

 * We are using a false discovery rate (FDR) cutoff of `r FDR`. 
 * We are not going to apply any additional cutoff in selecting differentially expressed genes (no log fold change minimum).

Read in the TAIR 10 file to get the descriptions for each gene.  This information will be added to the results file for reference.
```{r}
annots=getAnnots() # getAnnots() is defined in ProjectWide.R, see Technical Details section
```

Read in the raw counts per gene.
```{r}
rawCounts = getRawCounts() #defined in ProjectWide.R, see Technical Details section
## Change column names to short names to use in R
# get short names from the Samples.txt ref table
samples=getSampleNames()
# make sure the columns in rawCounts are in the same order as the Samples.txt ref table
names=getSampleFiles(samples)
rawCounts=rawCounts[names] 
# translate names in rawCounts table
names(rawCounts)=samples
```

Convert the raw counts to a DGElist. A DGElist (Differential Gene Expression list) is the format EdgeR uses for handling data sets.
```{r}
dge <- DGEList(counts=rawCounts, 
               genes=row.names(rawCounts), 
               group=getSampleGroups(samples), 
               remove.zeros=T)
```

Note: The genes with 0-counts are genes that have zero counts across ALL samples.  Additional genes may have zero counts within some groups.

Calculate normalization factors.  There are multiple ways to do this.
Here we use the RLE method which is described in the EdgeR users guide as "_method="RLE" is the scaling factor method proposed by Anders and Huber (2010). We call it "relative log expression", as median library is calculated from the geometric mean of all columns and the median ratio of each sample to the median library is taken as the scale factor._"
```{r}
dge <- calcNormFactors(dge, method="RLE")
```
```{r echo=FALSE}
#kable(dge$samples[samples,], row.names=T, digits=4, align="l")
```
The normalization factors range from `r round(min(dge$samples[samples,"norm.factors"]),3)` to `r round(max(dge$samples[samples,"norm.factors"]),3)`.

## Data overview

Plot MDS.
A multi-dimensional scaling plot is a 2-dimensional scatter plot where distances between samples are meant to approximate the typical log2 fold changes between the samples.  It allows us to summarize general trends of similarities between samples.

```{r fig.width=5, fig.height=5}
plotMDS(dge, col=getSampleColors(colnames(dge$counts)), main="MDS plot", las=1)
quartz.save(file="results/MDSplot.png",dpi=600,height=6,width=6)
```

Dim 1 cleanly separates the samples by time of collection.  In both dimensions, treatments and controls are intermingled.  Dim 2 shows mostly clustered groups with a couple samples being separated from their groups, such as T8.4, T4.4 and T2.1.

Plot MDS per time point.

```{r fig.height=3, fig.width=9}
par(mfrow=c(1,3))
timeSets = list(at2hours = grep("[CT]2", colnames(dge$counts)),
                at4hours = grep("[CT]4", colnames(dge$counts)),
                at8hours = grep("[CT]8", colnames(dge$counts)))
for (ts in names(timeSets)){
  dgeTemp=dge[,timeSets[[ts]]]
  plotMDS(dgeTemp, col=getSampleColors(colnames(dgeTemp$counts)), main=paste("MDS plot", ts), las=1)
}
```


Hierarchical clustering. Similar to the MDS plot, greater distances in the plot represent greater differences between samples.

```{r fig.width=5, fig.height=3}
normalized.counts=cpm(dge)
transposed=t(normalized.counts) # transposes the counts matrix
distance=dist(transposed) # calculates distance
clusters=hclust(distance) # does hierarchical clustering
#plot(clusters) # plots the clusters as a dendrogram
#defined in DrawPlots.R, see Technical Details section
par(mar=c(2,0,3,0))
makeClusterPlot(clusters, lab.col=getSampleColors()) 
```

Again, samples separate primarily by time of collection. One sample (C2.4) is an exception, clustering closer to the 4 hour group than the 2 hour group. Within the three major clusters, treatment and control samples are intermingled.

# Differential expression analysis

## Analyze each time point as an isolated experiment
Use the edgeR package to determine the log (base 2) fold change of each gene and the FDR corrected p-value indicating how likely it is that the gene's expression level _is_ different between the two treatments.

Some samples were removed from the analysis.
```{r}
d=getSampleTable()
sg=getSampleGroups()
names(sg)[grep("2|4|8", sg, invert=T)]
```


### 2 hours
Consider only the samples from the 2-hour time point.

```{r fig.width=4, fig.height=4}
#function defined in DetermineDE.R, see Technical Details section
dge2 <- make2groupMDS(imageFile="results/MDSat2hours.png",
                   Tr="T2",Cn="C2",
                   counts=rawCounts[,grep("2", getSampleTable()$SampleGroup)])
```

```{r}
#function defined in DetermineDE.R, see Technical Details section
#dex2 <- calcDEgenes(dge2)
d2=d[grep("[CT][2]",d$SampleGroup),]
res2 = calcDEgenesGLM(rawCounts, d2, showDesign=T)
```

Convert to a data frame.
```{r}
#function defined in DetermineDE.R, see Technical Details section
#res2 <- convertDEXtoDF(dex2, annots)
```

Consider different FDR values.

```{r echo=F, fig.height=4, fig.width=6}
plotFdrSelectionGuide(fdrColumn = res2$fdr, currentFDR=FDR, subtitle="2-hour time point")
```


`r sum(res2$fdr <= FDR)` genes have an FDR of `r FDR` or less.

```{r echo=F, fig.height=3, fig.width=5}
hist(res2$logFC[res2$fdr<=FDR], main="histogram of logFC values of DE genes", las=1, xlab="logFC")
```


### 4 hours
Consider only the samples from the 4-hour time point.

```{r fig.width=4, fig.height=4}
#function defined in DetermineDE.R, see Technical Details section
dge4 <- make2groupMDS(imageFile="results/MDSat4hours.png",
                   Tr="T4",Cn="C4",
                   counts=rawCounts[,grep("4", getSampleTable()$SampleGroup)])
```

```{r}
#function defined in DetermineDE.R, see Technical Details section
#dex4 <- calcDEgenes(dge4)
d4=d[grep("[CT][4]",d$SampleGroup),]
res4 = calcDEgenesGLM(rawCounts, d4, showDesign=T)
```

Convert to a data frame.
```{r}
#function defined in DetermineDE.R, see Technical Details section
# res4 <- convertDEXtoDF(dex4, annots)
```

Consider different FDR values.

```{r echo=F, fig.height=4, fig.width=6}
plotFdrSelectionGuide(fdrColumn = res4$fdr, currentFDR=FDR, subtitle="4-hour time point")
```

`r sum(res4$fdr <= FDR)` genes have an FDR of `r FDR` or less.


```{r echo=F, fig.height=3, fig.width=5}
hist(res4$logFC[res4$fdr<=FDR], main="histogram of logFC values of DE genes", las=1, xlab="logFC")
```


### 8 hours
Consider only the samples from the 8-hour time point.

```{r fig.width=4, fig.height=4}
#function defined in DetermineDE.R, see Technical Details section
dge8 <- make2groupMDS(imageFile="results/MDSat8hours.png",
                   Tr="T8",Cn="C8",
                   counts=rawCounts[,grep("8", getSampleTable()$SampleGroup)])
```

```{r}
#function defined in DetermineDE.R, see Technical Details section
# dex8 <- calcDEgenes(dge8)
d8=d[grep("[CT][8]",d$SampleGroup),]
res8 = calcDEgenesGLM(rawCounts, d8, showDesign=T)
```

Convert to a data frame.
```{r}
#function defined in DetermineDE.R, see Technical Details section
# res8 <- convertDEXtoDF(dex8, annots)
```

Consider different FDR values.

```{r echo=F, fig.height=4, fig.width=6}
plotFdrSelectionGuide(fdrColumn = res8$fdr, currentFDR=FDR, subtitle="8-hour time point")
```

`r sum(res8$fdr <= FDR)` genes have an FDR of `r FDR` or less.

```{r echo=F, fig.height=3, fig.width=5}
hist(res8$logFC[res8$fdr<=FDR], main="histogram of logFC values of DE genes", las=1, xlab="logFC")
```

##glm across time points

Create a design matrix giving the experimental factors for each sample.  Treatment is the main factor we are interested in.  We know time of collection affects expression data, but we want to use all the samples together, without simply detecting differences due to time.  To address this, we pass time of collection as a factor, but it is not the factor we test agains.  Flat is a factor we don't care about at all.  However, we suspect there may be flat-based batch effects, so we pass flat as a factor as well, so that it is accounted for in the model without being the factor we test against.
```{r}
d.all=d[grep("[CT][248]",d$SampleGroup),]
res.all = calcDEgenesGLM(rawCounts, d.all, showDesign=T)
```

Consider different FDR values.

```{r echo=F, fig.height=4, fig.width=6}
plotFdrSelectionGuide(fdrColumn = res.all$fdr, currentFDR=FDR, 
                      subtitle="GLM test using all samples")
```

`r sum(res.all$fdr <= FDR)` genes have an FDR of `r FDR` or less.

Get the names of the DE genes
```{r}
glmDEgenes = res.all$gene[res.all$fdr <= FDR]
```

```{r echo=F, fig.height=3, fig.width=5}
hist(res.all$logFC[res.all$fdr<=FDR], main="histogram of logFC values of DE genes", las=1, xlab="logFC")
```

# Results Summary

```{r}
DElist=list(at2hours = as.character(res2[res2$fdr<=FDR,"gene"]),
         at4hours = as.character(res4[res4$fdr<=FDR,"gene"]),
         at8hours = as.character(res8[res8$fdr<=FDR,"gene"]),
         glm = glmDEgenes)
allDEgenes = unique(unlist(DElist))
atDEgenes = unique(unlist(DElist[1:3]))
```


Using an FDR of `r FDR`, we found:

* `r sum(res2$fdr <= FDR)` differentially expressed genes at 2 hours,
* `r sum(res4$fdr <= FDR)` differentially expressed genes at 4 hours,
* `r sum(res8$fdr <= FDR)` differentially expressed genes at 8 hours.

These three time points include `r length(atDEgenes)` unique genes.

We found `r length(glmDEgenes)` DE genes using linear modeling, bringing the full total to `r length(allDEgenes)`.

## MDS of only DE genes

As a small sanity check, lets consider _only_ the genes that were DE in at least one comparison. The treatments and controls did no segregate when we considered all genes, but they should surely segregate if we only consider the DE genes.
```{r fig.width=4, fig.height=4, echo=F}
dgePost <- DGEList(counts=rawCounts[allDEgenes,], 
               genes=allDEgenes, 
               group=getSampleGroups(samples), 
               remove.zeros=T)
dgePost <- calcNormFactors(dgePost, method="RLE")
plotMDS(dgePost, col=getSampleColors(colnames(dgePost$counts)), 
        main="MDS plot using only DE genes", las=1, top = length(allDEgenes))
```

When we only consider the DE genes, the samples do segregate by treatment (as expected).  This set of genes also still segregates the samples by time of collection.  This may be a sign that many of the targets of ANT also regulated by circadian rhythms.

Only the glm genes.
```{r fig.width=7, fig.height=4, echo=F}
par(mfrow=c(1,2))

# the genes DE at a given time point
dgePost <- DGEList(counts=rawCounts[atDEgenes,], 
               genes=atDEgenes, 
               group=getSampleGroups(samples), 
               remove.zeros=T)
dgePost <- calcNormFactors(dgePost, method="RLE")
plotMDS(dgePost, col=getSampleColors(colnames(dgePost$counts)), 
        main="only DE genes from individual times", las=1, top = length(atDEgenes))

# The genes DE using glm
dgePost <- DGEList(counts=rawCounts[glmDEgenes,], 
               genes=glmDEgenes, 
               group=getSampleGroups(samples), 
               remove.zeros=T)
dgePost <- calcNormFactors(dgePost, method="RLE")
plotMDS(dgePost, col=getSampleColors(colnames(dgePost$counts)), 
        main="only DE genes from GLM method", las=1, top = length(glmDEgenes))
```

Among the genes determined by the linear modeling method the time-point segregation is less apparent.  The treatment-based segregation is similar.

## Venn

Are the same genes DE at the three different time points?

```{r echo=F, fig.width=3, fig.height=3}
suppressPackageStartupMessages(library(gplots))
par(oma=c(0,0,0,0), mar=c(0,0,0,0))
ven=venn(DElist[1:3])
```

There are very few DE genes in the 2-hour time point (`r sum(res2$fdr <= FDR)`) and most of those are also represented in one or both of the other time points.


Create a column to indicate in which time points a gene was DE.
```{r}
whichDE=data.frame(at2hour=ifelse(annots$gene %in% DElist$at2hour, "2", "x"),
                   at4hour=ifelse(annots$gene %in% DElist$at4hour, "4", "x"),
                   at8hour=ifelse(annots$gene %in% DElist$at8hour, "8", "x"))
whenDE=apply(whichDE, 1, paste0, collapse=".")
names(whenDE)=annots$gene
```
An x indicates that a gene is not DE. For example "2.x.8" means a gene is DE in the 2-hour and 8-hour time point, but not at the 4-hour time point, while "2.4.8" is DE in all time points.

Are the genes determined using linear modeling the same genes that were detected at individual time points?

```{r echo=F, fig.width=5, fig.height=4}
par(oma=c(0,0,0,0), mar=c(0,0,0,0))
ven=venn(DElist)
glmOnly = res.all[attr(ven,"intersections")$glm, "gene"]
allShare = attr(ven,"intersections")$`at2hours:at4hours:at8hours:glm`
```

`r length(glmOnly)` of the the `r length(glmDEgenes)` genes determined by the glm method were not found to be DE at any of the individual time points.

The `r length(allShare)` genes that are DE in all 4 comparisons are: `r allShare`.


## Heat map

### LogFC of DE genes

Make a single data frame with the log fold changes from all three comparisons.
```{r}
# fc for Fold Changes
fc = merge(data.frame(gene=res2$gene, at2=res2$logFC), 
            data.frame(gene=res4$gene, at4=res4$logFC), all=T)
fc = merge(fc, data.frame(gene=res8$gene, at8=res8$logFC), all=T)
row.names(fc)=fc$gene
fc=fc[, c("at2", "at4", "at8")]
```

Use whenDE and clustering based on logFC values, to create a gene order.
```{r}
# order DE genes based on clustering
fcDE = fc[allDEgenes,]
distance=dist(fcDE) # calculates distance # note the lack of transpose step
hc=hclust(distance) # does hierarchical clustering
labs=hc$labels[hc$order]

# Group DE genes by when they are DE
labswhenDE=whenDE[labs]
geneOrd=labs[order(labswhenDE)]

# make a vector to draw lines between groups
wdg=table(labswhenDE)
for (i in 1:length(table(labswhenDE))){
  wdg[i]=sum(table(labswhenDE)[1:i])
}
wdg = length(labswhenDE)-wdg
wdg = wdg + 0.5 # the plot function I'm using offsets by .5
```

Groups are in this order:
`r names(wdg)`


Make a heatmap of the log fold changes of DE genes. 
```{r echo=FALSE, fig.width=6, fig.height=13}
# plot
fc=fc[geneOrd,]
m=max(abs(fc), na.rm=T)
fcm=as.matrix(fc)
fcm[fcm>m]=m
plotChart(chart=fcm, width=14, range=m, mar=c(1.2,0,1,0),
          showText=F, main="Log Fold Change of DE genes")
abline(h=wdg, lwd=.5, col="gray")
text(x=par("usr")[2], y=wdg, adj=c(1.5,0), labels=names(table(labswhenDE)), xpd=T)
```

```{r echo=FALSE, fig.width=6, fig.height=1}
plotChart(matrix(round(seq(-m,m,length.out=21),1), nrow=1, ncol=21, byrow=T), 
          main="log fold change color legend", 
          mar=c(0,0,1,0), width = 1, range=m, cex.text=.9)
```

Note that the above chart includes all fold change values for all genes that were DE in any of the three comparisons, so many of the values shown are not 'significant'.  Even so, the trends are generally consistent across the time points.

### Expression of DE genes

Plot the expression values. Plot the genes in the same order as the above plot.

```{r fig.width=8, fig.height=12, echo=F}
# get RPKM counts
rpkm=getRPKM(getSampleNames(), ave=F, sd=F)

# # order DE genes based on clustering
# normalized.countsDE=normalized.counts[allDEgenes,]
# distance=dist(normalized.countsDE) # calculates distance # note the lack of transpose step
# hc=hclust(distance) # does hierarchical clustering
# geneOrd=hc$labels[hc$order]

# keep only the rpkm values for the DE genes, and order them
rpkm=rpkm[geneOrd,]

m=200
rpkm=as.matrix(rpkm)
median=round(median(rpkm),0)
rpkm[rpkm>m]=m
rpkm = rpkm-median #shift the values so values below median are negative
plotChart(chart=as.matrix(rpkm), width=5, range=m, mar=c(1.2,0,1,0),
          showText=F, main="RPKM values of DE genes")
abline(h=wdg, lwd=.5, col="black")
text(x=par("usr")[2], y=wdg, adj=c(1.5,0), labels=names(table(labswhenDE)), xpd=T)
```

```{r echo=FALSE, fig.width=6, fig.height=1}
leg=matrix(seq(0,m,length.out=21), nrow=1, ncol=21, byrow=T)
legCols=leg-median
legText=leg
legText[length(leg)]=paste0(leg[length(leg)], "+")
plotChart(chart=legCols, chartText=legText, main="RPKM color legend", 
          mar=c(0,0,1,0), width = 1, range=m, cex.text=.9)
abline(v=sum(legCols<0)+.5, xpd=FALSE)
text(x=sum(legCols<0)+.5, y=-.3, pos=4, labels=paste("median value = ", median, "(white)"), xpd=T)
```

The section of genes labeled "x.x.x" are the genes that are not DE in any of the individual time points but they were found to be DE in the linear modeling method.

Make a vector with genes names as names and values indicating that genes row in the heat maps.
```{r}
HeatmapRow = 1:length(geneOrd)
names(HeatmapRow) = geneOrd
```


# Write Results

The extended results file will include the rows for all genes (even those with an FDR above `r FDR`), and results for all comparisons and additional reference data such as RPKM values and gene descriptions.  We will also make a second file for each comparison that is designed for use with web tools such as AgriGO or AraCyc.  It will only include the genes that were DE (using FDR `r FDR`) in a given comparison and only include columns for the gene id and log fold change.

Get the RPKM values for all sample groups
```{r}
rpkm=getRPKM(ave=T, sd=F)
rpkm=rpkm[,grep("ave",names(rpkm))] #remove this line to include inividual sample values in results file.
```

## Extended results

The extended results include:

 * gene id
 * log2 fold change (treatment vs mock, for each comparison)
 * fdr (for each comparison)
 * mean RPKM for the sample groups
 * _whenDE_ - a quick reference column indicating which of the three comparisons found a given gene to be DE.
 * _HeatmapRow_ - a number indicating which row of the heat maps (above) corresponds to a given gene.
 * gene info (symbol and description)
 
Put the results in a format that will make this easier.  A set of similar results is easiest to handle as a list.
```{r}
resList=list(res2, res4, res8, res.all[names(res8)])
#names(resList)=names(timeSets)
names(resList) = c(names(timeSets), "GLM")
resList1 = resList
# change names so columns will still be unique after mergeing
for (comp in names(resList)){
  n=ncol(resList[[comp]])
  names(resList[[comp]])[2:n] = paste(names(resList[[comp]])[2:n], comp, sep=".")
}
sapply(resList, nrow) #give the total rows from each results table.
```

The total rows may be different for each time point, because of genes that had zero counts in all samples for one time point (and were removed from the comparison) but had counts in another time point (so they were included). These genes will have NA values for fdr and logFC in comparisons where they had 0 counts. Each case could include results for up to `r nrow(annots)` genes (the total number of genes in the annotations file).


Merge all results, rpkm values, and gene annotation info.
```{r}
# merge all fdr and logFC values.
res=merge(resList[[1]], resList[[2]], by="gene", all=T)
res=merge(res, resList[[3]], by="gene", all=T)
res=merge(res, resList[[4]], by="gene", all=T)
# not all columns are meaningful
keepCols = grep("gene|logFC|fdr|PValue", names(res))
res = res[, keepCols]
# add a column indicated which comparison(s) found a gene to be DE
res$whenDE=whenDE[as.character(res$gene)] 
# add a column indicating which row in the heat maps is for each gene
res$HeatmapRow = HeatmapRow[as.character(res$gene)]
# add mean rpkm values for each sample group (means exclude any exluded samples)
res=merge(res, rpkm, by.x="gene", by.y=0, all.x=T)
# add the symbol and gene description from the annotation file
res=merge(res, annots)

# put rows in a logical order
res=res[order(res$whenDE, res$HeatmapRow, res$gene),]
```

Write results to a file.
```{r}
fname="results/DifferentialExpression-all.txt"
write.table(x=res, file=fname, quote=F, sep="\t", row.names=F, col.names=T)
system(paste("gzip -f", fname))
```



## DE genes list

The DE-only results include:

 * gene id
 * log fold change (rounded to 3 digits after the decimal)
 
Rows are ordered by logFC, with most increased at the top and most decreased at the bottom.  Only genes with an FDR of `r FDR` or less are included.
```{r}
for (basename in names(resList1)){
  #function defined in DetermineDE.R, see Technical Details section
  WriteShortResults(basename, res=resList1[[basename]], de_FDR=FDR)
  #
  print(paste(rep("-", 50), collapse="")) # spacer for comments
}
```

Unlike the extended results, the short results do not include columns names.



# Conclusions

**Which genes are differentially expressed in response to dex treatment at 2 hours? at 4 hours? or at 8 hours?**
The DE genes for each time point are listed in the results files called "DifferentialExpression-"

**Which genes are differentially expressed at multiple time points?**
This is indicated in each of the extended results files in the whenDE column.  The number of DE genes shared by each time point is also shown visually in the Venn diagram (above).  In short:
```{r echo=FALSE}
table(whenDE)
```

The treated samples are not very different from the control They do not segregate in the MDS plot or hierarchical clustering unless we consider only the genes that we picked out as different between treatment and control.  

However, we are confident that the treatment was detected by the plant because Beth observed the plants for several days after treatment, and saw a difference in phenotype between the dex-treated and the mock-treated groups.  So we can rule out an ineffective treatment (cases where the native copy of ANT was active enough to conceal any induced effects, or the GR construct is not effectively inhibiting the abundant ANT in the absence of dex, etc).  

If the genes we have identified are truly responding to ANT activation, having a relatively short list will keep us focused on the genes that are truly affected. 

# Technical Details

Session Info - list packages used in this document and the version numbers for all software and packages.
```{r}
sessionInfo()
```

Functions defined outside of this document.
```{r}
makeClusterPlot
make2groupMDS
calcDEgenes
convertDEXtoDF
plotChart
getAnnots
getRPKM
WriteLongResults
WriteShortResults
```


