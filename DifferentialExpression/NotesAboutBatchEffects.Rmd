---
title: "Notes After Batch Effects"
output:
  html_document:
    toc: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```





```{r}








# Among our "DE genes" 
# we should see that the coefficient for the treatment is much 
# greater than the coefficient for the flat.  ...Right? At least most of the time?


# use the code from the calcDEgenesGLM function and the d2 input object
# get to the fit object

# The 140 genes we ultimatley considered DE from this comparison are:
de2=res2$gene[res2$fdr<=FDR]

# in general, is the treatment coefficient higher?
boxplot(fit$coefficients)
# no

# how about specifically in the DE genes?
boxplot(fit$coefficients[de2,])
# not really

# General question, why are the intercepts negative?

# boxplots might over-summerize bipolar data, lets see the individual points for DE genes.
# -----not sure if this is plotted correctly
plot(jitter(rep(1:5, each=length(de2))), as.matrix(fit$coefficients[de2,]))

# how does the flat coefficient compare to the corresponding treatment coef?
plot(fit$coefficients[de2,"TreatmentDEX"],fit$coefficients[de2,"flat4"])
points(fit$coefficients[de2,"TreatmentDEX"],fit$coefficients[de2,"flat3"], col="blue")
points(fit$coefficients[de2,"TreatmentDEX"],fit$coefficients[de2,"flat2"], col="purple")

goiFromDE2.good = de2[which(fit$coefficients[de2,"TreatmentDEX"]< -2)]
goiFromDE2.bad = de2[which(fit$coefficients[de2,"flat4"]< -2)]




# Maybe we can reduce the factors here.  use "flat12" vs "flat34" or something like that.
# lets see if any one or two flats are more correlated and could be merged.
plot(data.frame(fit$coefficients))
# Treatment doesn't look like it has any favorites.
# just look at flats
plot(data.frame(fit$coefficients[,2:4]))
# are the same points extreme in all plots? maybe flat 1 is outlier?
cor(data.frame(fit$coefficients[,2:4]))
cols=rep("black", nrow(fit$coefficients))
cols[which(fit$coefficients[,"flat2"]>2)] = "red"
cols[which(fit$coefficients[,"flat2"]< -2)] = "blue"
plot(data.frame(fit$coefficients[,2:4]), col=cols)
# Look at the flat3 vs flat4 plot. 
# All the poinst that are blue in the lower left quadrant, or red in the upper right,
# represent genes where all three flats (2, 3, and 4) are changed in the same direction relative to flat1.

# Would the coefficients be generally lower if a different flat were used as the reference?
flat=factor(d$Replicate.Flatpair., levels=c(2,1,3,4))
# re-run lines 44 - 64 of DetermineDE (the calcDEgenesGLM function from defining flat to return, exlusive)

# Now do any of the flats look correlated?
cor(data.frame(fit$coefficients[,2:4]))
cols=rep("black", nrow(fit$coefficients))
cols[which(fit$coefficients[,"flat1"]>2)] = "red"
cols[which(fit$coefficients[,"flat1"]< -2)] = "blue"
plot(data.frame(fit$coefficients[,2:4]), col=cols)


flat=factor(d$Replicate.Flatpair., levels=c(3,1,2,4))
# re-run lines 44 - 64 of DetermineDE (the calcDEgenesGLM function from defining flat to return, exlusive)

de2.ref.flat1 = de2
de2.ref.flat3 = tT$gene[tT$fdr<=FDR]
length(de2.ref.flat1)
length(de2.ref.flat3)
length(intersect(de2.ref.flat1,de2.ref.flat3))
# ok, we get the same list of DE genes using flat1 as the ref or flat3, so the choice of ref doesn't matter (as expected)


# what if we group pairs of replicates?
newFlats = d$Replicate.Flatpair.
newFlats = gsub("1|2", "12", newFlats)
newFlats = gsub("3|4", "34", newFlats)
flat=factor(newFlats)
# re-run lines 44 - 64 of DetermineDE (the calcDEgenesGLM function from defining flat to return, exlusive)

de2.ref12 = tT$gene[tT$fdr<=FDR]

length(de2.ref.flat1)
length(de2.ref12)
length(intersect(de2.ref.flat1,de2.ref12))

# only 12 DE genes (all of which are in the original group of 140)
# this reduction in DE genes is probably because of the increased variation 
# in each group from combingin separate flats.







### Flag genes whose expression is changed due to treatment, but changed MORE do to flat
coefs = data.frame(fit$coefficients)
#coefs$maxFlat=apply(coefs[,2:4],1,max)
coefs$maxFlat=apply(coefs[,2:4],1,FUN=function(x){max(abs(x))})
#coefs$maxFlat=apply(coefs[,2:4],1,FUN=function(x){mean(abs(x))})
coefs$cols = rep("black", nrow(coefs))
#names(cols) = row.names(coefs)
deGLM=row.names(res.all)[res.all$fdr<=FDR]
coefs[deGLM,"cols"] = "blue"

# plot(coefs[de2,"TreatmentDEX"], coefs[de2, "maxFlat"])
plot(abs(coefs[de2,"TreatmentDEX"]), abs(coefs[de2, "maxFlat"]), col=coefs[de2,"cols"])
abline(h=0, v=0, a=0, b=1, lty=2, col="gray")
plot(abs(coefs[de2,"TreatmentDEX"]), abs(coefs[de2, "maxFlat"]), col=coefs[de2,"cols"], xlim=c(0,1), ylim=c(0,1))
abline(h=0, v=0, a=0, b=1, lty=2, col="gray")

# sort de2 (the list of de genes at2hour) by the ratio of change due to dex vs change due to flat
de2 = de2[order(abs(coefs[de2,"maxFlat"])/abs(coefs[de2,"TreatmentDEX"]),decreasing = T)]
head(fit$coefficients[de2,], 10)
tail(fit$coefficients[de2,], 10)



#recall that # tT <- data.frame(topTags(lrt, n=nrow(lrt$table)), adjust.method="BH")
# coefficients are stored in the DGEGLM object as natural log values, but they are reported as log2 values.
plot(exp(fit$coefficients[tT$genes,5]), 2^(tT$logFC))
# Thus, these coefficents make more sence:
head(exp(fit$coefficients))

## recall that strangely misleading boxplot from up top, that had a negative intercept value
# see now:
boxplot(exp(fit$coefficients[de2,]))







```

