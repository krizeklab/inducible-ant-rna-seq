---
title: "Direction Of Change"
author: "I. Blakley"
output: html_document
---

```{r setup, echo=FALSE}
library(knitr)
#knitr::opts_chunk$set(fig.path = 'results/figure')
knitr::knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",", digits=2)
})
source("../src/ProjectWide.R")
source("../src/DrawPlots.R")
source("../CompareToANTAIL6mutant/src/SquareVennClover.R") #TODO move this function
source("../CompareToANTAIL6mutant/src/VennClover.R") #TODO move this function
```

# Introduction

Not all of the DE genes are direct targets of ANT.  Many must be indirect targets of ANT.  It is very possible that the difference we see (the expression level being higher or lower in the treatment samples relative to the control) are actually the reverse of the difference that is directly cuased by ANT.  Nature often employs negative feedback loops to maintain stable levels of hormones, proteins, gene expression, etc.  It is very possible that there are direct targets of ANT whose expression is re-stablized so quickly that our experiment doesn't catch the difference.  It is also possible (albeit less likely) that the negative feedback loop even creates a recoil effect, pushing a genes expression level past the origal (control) level.  Natural feedback loops are probably calibrated to work best in natural conditions, where an increase in ANT is accompanied by other related transcription factors and the level of ANT is sustained.  A good, stabilizing mechanism in that environment might be prone to recoil in the experiment where the plant recieves an acute dose of ANT.

Since we have sampled multiple time points, we can look for this recoil effect.  An earlier sample might catch the gene in its direct effect difference, while a later sample catches the same gene in its indirect difference in the opposite direction.  However, all samples might be before the full effect of ANT has set in or after the recoil has kicked in.  Thus, we can scan for examples that suggest the recoil effect, but we cannot prove the absense of it.

Question:

* Are there any genes that are differentially expressed in two or more time points, with the change being in opposite directions?

# Analysis

```{r echo=FALSE}
# # Get the annotation data to include in output files.
# annots=getAnnots()[2:3]
```

Get the differential expression results.
```{r}
FDR=getFDR()*10

# data frame of fdr values
agFDR=getDEtable(returnFDR=T, returnLogFC=F)
names(agFDR) = gsub("fdr.", "", names(agFDR))
totalDE = sum(apply(agFDR, 1, min, na.rm=T)<=FDR)
```

There are a total of `r totalDE` genes that had an FDR of `r FDR` or less in at least one time point.

Get the differentially expressed genes for each time point.
```{r}
#create empty list to hold DE results
comparisons=c("at2hours", "at4hours", "at8hours")
AGlist = as.list(comparisons) # AGlist for the list of Ant-Gr results.
names(AGlist)=comparisons

for (comp in comparisons){
  maxFDR=FDR
  names(maxFDR)=comp
  # function defined in ProjectWide.R
  d = getDEtable(comparison=comp, returnFDR=F, returnLogFC=T, maxFDR=maxFDR)
  names(d) = gsub(paste0(".", comp), "", names(d)) #simplify column name
  AGlist[[comp]] = d
}
```

How many genes were DE in each time point?
```{r}
lapply(AGlist, nrow)
```

Use a function to categorize each gene based on weather it is up or down in each of two time points.
```{r}
createCategoryDF = function(df1, df2){
  # function defined in VennClover.R
  getOverlapsTable(N=row.names(df1)[df1$logFC>0],
                   S=row.names(df1)[df1$logFC<0],
                   E=row.names(df2)[df2$logFC>0],
                   W=row.names(df2)[df2$logFC<0])
}
```


# 2 hour and 4 hour
**Genes DE in the same direction in both the 2- and 4-hour comparisons**
```{r}
# get the logFC values or all genes that are DE in both 2 and 4
same2n4 = merge(AGlist[["at2hours"]], AGlist[["at4hours"]], by=0)
# which ones are DE in the same direction (both pos logFC or neg logFC)
w = which(with(same2n4, logFC.x*logFC.y>0))
revW24 = which(with(same2n4, logFC.x*logFC.y<0))
# get the gene ids for those rows
same2n4genes = same2n4$Row.names[w]
```

**`r length(same2n4genes)`** genes were DE in the same direciton in both time points.              
**`r length(revW24)`** genes were DE in the opposite direciton.


```{r echo=F}
# # Use these gene ids to gather data from various sources
# rpkm=getRPKM()
# toWrite2n4 = cbind(gene=same2n4genes,
#                    getDEtable()[same2n4genes,],
#                    #antail6[same2n4genes,c("logFC", "fdr", "w.rpkm", "m.rpkm")],
#                    rpkm[same2n4genes, grep("ave|sd", names(rpkm))],
#                    annots[same2n4genes,])
# # names(toWrite2n4)[names(toWrite2n4)=="logFC"] = "logFC.antail6"
# # names(toWrite2n4)[names(toWrite2n4)=="fdr"] = "fdr.antail6"
# fname24 = "results/SameDirectionIn2and4hour.txt"
# write.table(toWrite2n4, fname24, sep="\t", quote=F, col.names = T, row.names = F)
# 
# #This subset of genes includes `r sum(toWrite2n4$logFC.at2hour>0)` that are up in both comparisons, and `r sum(toWrite2n4$logFC.at2hour<0)` that are down in both comparisons.
```


```{r echo=F}
df2n4 = createCategoryDF(AGlist[["at2hours"]], AGlist[["at4hours"]])
vals2n4 = getCloverValues(df2n4)
cols24 = getSampleGroupColors(c("T2", "C2", "T4", "C4"))
fname="figure/at2and4hours.png"
draw4CircleVenn(counts=vals2n4, 
                labels=c("Up at2hours", "Down at2hours", 
                         "Up at4hours", "Down at4hours"), 
                fname=fname, main="early time points",
                fg.col=cols24, bg.col=cols24, bg.alpha=.5)
```


# 4 hour and 8 hour
**Genes DE in the same direction in both the 4- and 8-hour comparisons**

Use the same steps as the 2 vs 4 comparison.
```{r echo=FALSE}
# get the logFC values or all genes that are DE in both 4 and 8
same4n8 = merge(AGlist[["at4hours"]], AGlist[["at8hours"]], by=0)
# which ones are DE in the same direction (both pos logFC or neg logFC)
w = which(with(same4n8, logFC.x*logFC.y>0))
revW48 = which(with(same4n8, logFC.x*logFC.y<0))
# get the gene ids for those rows
same4n8genes = same4n8$Row.names[w]
```

**`r length(same4n8genes)`** genes were DE in the same direciton in both time points.              
**`r length(revW48)`** genes were DE in the opposite direciton.

```{r echo=FALSE}
# # Use these gene ids to gather data from various sources
# # Use the same steps as the 2 vs 4 comparison.
# toWrite4n8 = cbind(gene=same4n8genes,
#                    getDEtable()[same4n8genes,],
#                    #antail6[same4n8genes,c("logFC", "fdr", "w.rpkm", "m.rpkm")],
#                    rpkm[same4n8genes, grep("ave|sd", names(rpkm))],
#                    annots[same4n8genes,])
# # names(toWrite4n8)[names(toWrite4n8)=="logFC"] = "logFC.antail6"
# # names(toWrite4n8)[names(toWrite4n8)=="fdr"] = "fdr.antail6"
# fname48 = "results/SameDirectionIn4and8hour.txt"
# write.table(toWrite4n8, fname48, sep="\t", quote=F, col.names = T, row.names = F)
# 
# #This subset of genes includes `r sum(toWrite4n8$logFC.at4hour>0)` that are up in both comparisons, and `r sum(toWrite4n8$logFC.at4hour<0)` that are down in both comparisons.
```

```{r echo=F}
df4n8 = createCategoryDF(AGlist[["at4hours"]], AGlist[["at8hours"]])
vals4n8 = getCloverValues(df4n8)
cols48 = getSampleGroupColors(c("T4", "C4", "T8", "C8"))
fname="figure/at4and8hours.png"
draw4CircleVenn(counts=vals4n8, 
                labels=c("Up at4hours", "Down at4hours", 
                         "Up at8hours", "Down at8hours"), 
                fname=fname, main="latter time points",
                fg.col=cols48, bg.col=cols48, bg.alpha=.5)
```





# 2 hour and 8 hour
**Genes DE in the same direction in both the 2- and 8-hour comparisons**

Use the same steps as the 2 vs 4 comparison.
```{r echo=FALSE}
# get the logFC values or all genes that are DE in both 2 and 8
same2n8 = merge(AGlist[["at2hours"]], AGlist[["at8hours"]], by=0)
# which ones are DE in the same direction (both pos logFC or neg logFC)
w = which(with(same2n8, logFC.x*logFC.y>0))
revW28 = which(with(same2n8, logFC.x*logFC.y<0))
# get the gene ids for those rows
same2n8genes = same2n8$Row.names[w]
```

**`r length(same2n8genes)`** genes were DE in the same direciton in both time points.              
**`r length(revW28)`** genes were DE in the opposite direciton.


```{r echo=FALSE}
# # Use these gene ids to gather data from various sources
# # Use the same steps as the 2 vs 4 comparison.
# 
# toWrite2n8 = cbind(gene=same2n8genes,
#                    getDEtable()[same2n8genes,],
#                    #antail6[same2n8genes,c("logFC", "fdr", "w.rpkm", "m.rpkm")],
#                    rpkm[same2n8genes, grep("ave|sd", names(rpkm))],
#                    annots[same2n8genes,])
# # names(toWrite2n8)[names(toWrite2n8)=="logFC"] = "logFC.antail6"
# # names(toWrite2n8)[names(toWrite2n8)=="fdr"] = "fdr.antail6"
# fname28 = "results/SameDirectionIn2and8hour.txt"
# write.table(toWrite2n8, fname28, sep="\t", quote=F, col.names = T, row.names = F)
# 
# # This subset of genes includes `r sum(toWrite2n8$logFC.at2hour>0)` that are up in both comparisons, and `r sum(toWrite2n8$logFC.at2hour<0)` that are down in both comparisons.
```

```{r echo=F}
df2n8 = createCategoryDF(AGlist[["at2hours"]], AGlist[["at8hours"]])
vals2n8 = getCloverValues(df2n8)
cols28 = getSampleGroupColors(c("T2", "C2", "T8", "C8"))
fname="figure/at2and8hours.png"
draw4CircleVenn(counts=vals2n8, 
                labels=c("Up at2hours", "Down at2hours", 
                         "Up at8hours", "Down at8hours"), 
                fname=fname, main="outer time points",
                fg.col=cols28, bg.col=cols28, bg.alpha=.5)
```


# Conclusion

Notice the 0's in the upper left and lower right of every combination.  
This means that if we use an FDR of 0.001 for all time opints, there is no overlap between genes that were up-regulated (at any of the collection times) and genes that were down-regulated (at any of the collection times).

Using an FDR of 0.01, there are two genes with opposite regulation between the 2-hour and 4-hour time points, while there are 121 genes regulated in the same direction.
At that FDR, the 2-hour time point has 317 DE genes, and we expect about 3 to be false discoveries.
And the 4-hour time point has 530 genes, thus tolerating 5 false discoveries.


```{r}
sessionInfo()
```

