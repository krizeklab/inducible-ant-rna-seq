#!/bin/bash

#addBedtools # make sure bedtools is on the path

FOLDER=GeneStructures
mkdir $FOLDER

# get 1 kb pomoter regions
# -s makes -l (left) and -r (right) defined based on strand (-l is upstream instead of left)
gunzip -c TAIR10.bed.gz | cut -f 1-6 | bedtools flank -s -l 1000 -r 0 -i - -g genome.txt > $FOLDER/promoter-1kb.bed

# get 1 kb downstream regions
gunzip -c TAIR10.bed.gz | cut -f 1-6 | bedtools flank -s -l 0 -r 1000 -i - -g genome.txt > $FOLDER/downstream-1kb.bed

# get individual exons
# really, bed12tobed6 is the one-step tool for this, but is not working, and this is. (based on bedtools version 2.26.0)
#gunzip -c TAIR10.bed.gz | cut -f 1-12 - | bedtools bedtobam -i - -g genome.txt -bed12 | bedtools bamtobed -split -i - | cut -f 1-4 - > $FOLDER/exons.bed



# get the introns -- not a ideal method
# To account for introns in one model overlapping the exons of another model, handle each gene model independently.
# This for-loop takes a long time to run (1-2 hours).

# get the range for each gene.
# gunzip -c TAIR10.bed.gz | cut -f 1-4 > range.bed

# touch $FOLDER/introns.bed
# for G in $(gunzip -c TAIR10.bed.gz | cut -f 4 | head -100); 
# do 
# # echo "The id is $G"; 
# grep $G range.bed > block.bed; 
# # grep $G $FOLDER/exons.bed > ex.bed; 
# grep $G $FOLDER/exons.bed | bedtools subtract -a block.bed -b - | cut -f 1-4 >> $FOLDER/introns.bed
# # bedtools subtract -a block.bed -b ex.bed | cut -f 1-4 >> $FOLDER/introns.bed
# # bedtools complement -split -g genome.txt -i exons.bed >> introns.bed
# # bedtools intersect -split -a block.bed -b exons.bed >> introns.bed
# done

# rm range.bed
# rm block.bed






