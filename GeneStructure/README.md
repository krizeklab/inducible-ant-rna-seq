# Gene Structure of AINTEGUMENTA and related genes

This repository contains data files and (mostly) R code used to analzye an RNA-Seq data set generated from an ANT-GR mutant. 

* * *

ANT is very highly expressed in this data set, but the alignment across the gene is not exactly what I expected. 

In IGB, in a lot of the samples, ANT really looks like two genes.  In 2 samples there is a lot of support for the 3rd intron, but in most, there is next to none. The latter pattern is true in the Col-0 samples I looked at, so its not a Ler-on-Col alignment issue. This could still be an alignment issue if a) the genome sequence is incorrect b) tophat is aligning reads over the small exon.  We would have to try other alignment methods to see if the alignment process can explain the oddity, but it seems odd that an alignment artifact would affect 2 samples differently than all the others.

This topic may be explored more at a later time.

* * *

## What's here 

* * *

### IGBimages

Images generated in IGB.

* * *

### GeneModels

Models generated based on alignment data to help visualize transcripts.

* * *


## Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Beth Krizek krizek@sc.edu
* Ivory Blakley ieclabau@uncc.edu

* * *

## License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT