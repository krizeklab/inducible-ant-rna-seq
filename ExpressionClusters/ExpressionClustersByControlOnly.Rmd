---
title: "C-only Expression Clusters"
author: "I. Blakley"
output:
  html_document:
    toc: true
    code_folding: show
    toc_float: true
---

```{r setup, echo=FALSE}
library(knitr)
library(reshape2)
library(cluster)
#knitr::opts_chunk$set(fig.path = 'results/figure')
knitr::knit_hooks$set(inline = function(x) {
	if (is.numeric(x)){
		x = prettyNum(x, big.mark=",", digits=2)
	}
	paste0("**", x, "**") # so generated values are always bold
})
source("../src/ProjectWide.R")
source("../src/DrawPlots.R")
source("src/Clusters.R")
base="HierExprControlOnly"
```

# Introduction

I want to define groups of genes that are similar based on their CPM based on only the control samples. I want to include genes that are DE and genes that are most certianly not DE.  

I want to use these groups to represent genes that are probably controlled by the same mechanisms.  I can use the DE genes in the group to link each cluster to clusters of DE genes that were grouped based on the C and T info.  After the clusters are linked across the two sets, I can remove the DE genes from this set of clusters, leaving me with several pairs of clusters where one cluster has genes that changed with treatment (in at least one time point) and the other cluster has genes that had similar expression in the control samples but did not respond to the treatment.  That could be used in some (still TBD) downstream process to find motifs.

The theory is that these genes that are similar in the control samples probably have similar motifs in their promoters. If I can split a set that is mostly similar but differ only in their response to ANT, that could be a way of identifiing the ANT promoter (or promoters of ANT's downstream actors).  If most clusters are predominantly DE or predominantly nonDE, then this won't work.  ANT is present in the plant even in the controls, so it may not be possible to isolate non-ANT factors like I am proposing here.

### Question
For this markdown, the aim is to make the clusters and answer this question:

 * *When genes are grouped based on their expression levels in the control samples only, to what extent do genes that respond to ANT segrate from those that do not?*

For a technical answer, give a histogram showing the distribution of the per-cluster %DE; does this look like one distribution or more-than-one?  Are some clusters outliers in this distribution?

For a biological answer, is it reasonable to think that we could use the control sample expression values to bring together genes whose regulator factors (ie, motifs in promoters) are similar *but that differ* in their response to ANT ?



To save some processing (and headache) I only want to worry about genes that are DE (that do change in at least one time point) or that are definitively notDE (do not change at any time point even with a slightly more liberal FDR than what was used to define DE genes).  In other words, genes that are on the borderline of differential expression should not participate.  

# Define clusters

### Input data

Get DE genes list.
```{r}
FDR = getFDR()
allFDR = rep(FDR, 3)
names(allFDR) = c("at2hours", "at4hours", "at8hours")
deTable = getDEtable(comparison = names(allFDR),
										 unionIntersect = "union",
										 maxFDR = allFDR)
```

The genes AT5G09463, AT5G09462, AT5G09460, and AT5G09461 all occupy the same range in the genome, so they count the exact same reads.  So these four genes will look like a particulary tight cluster.  Cases like this falsely inflate the Silhouette index and other metrics of good clusterings.  Remove any cases where all measurements of the gene, and the first part of the name are all duplicated.
```{r}
determienDuplicatesBy = paste(apply(deTable, 1, paste, collapse="_"),
															substr(row.names(deTable), 1, 7))
isDup = duplicated(determienDuplicatesBy)
dupGenes = row.names(deTable)[isDup]
deTable = deTable[!isDup,]
```
These `r length(dupGenes)` genes were removed because they have identical data (and a nearly identical name) to another gene: `r paste(dupGenes, collapse=", ")`.

```{r}
deGenes = row.names(deTable)
```

Using an FDR of `r FDR`, we considered `r length(deGenes)` genes to be DE in at least one time point.

Get the notDE genes list.
```{r}
# The getDEtable isn't exactly made for this, so this may seem round-about
# Get genes that DO meet the extra liberal FDR cuttoff
extraLibFdr = rep(0.8, 3) #.8 was chosen becuase I wanted to get about 10x as many notDE genes as DE genes, and I achieved that through trial and error.
names(extraLibFdr) = names(allFDR)
tempoTable = getDEtable(comparison = names(extraLibFdr),
										 unionIntersect = "union",
										 maxFDR = extraLibFdr)
possiblyMaybeCouldBeDEgenes = row.names(tempoTable)
# Get the results for all genes 
allGenes = getDEtable()
# only keep the entries for genes that we were able to test in all comparisons
allGenes = allGenes[grep("fdr.", names(allGenes))]
ableToTest = na.omit(allGenes)
# remove the genes that might maybe be DE
notDeGenes = setdiff(row.names(allGenes), possiblyMaybeCouldBeDEgenes)
```

There are `r length(notDeGenes)` that we will consider "notDE".  They did not have an fdr below `r unique(extraLibFdr)` any of the differential expression comparisons.

Put the DE genes and the notDE genes together for clustering.
```{r}
sortableGenes = c(deGenes, notDeGenes)
```

Now we will set about clustering these `r length(sortableGenes)` genes (`r length(deGenes)` DE genes and `r length(notDeGenes)` not-DE genes) based soley on their values in the control samples.

Get the RPKM values.
```{r}
rpkm = getRPKM(ave=T, sd=F)

#samples = getSampleNames()
#wideCounts = as.matrix(rpkm[deGenes, samples])

# use the averages, not all samples individually
slimCounts = as.matrix(rpkm[sortableGenes, grep("ave", names(rpkm))])
# use only the control sample values
slimCounts = slimCounts[,grep("C", colnames(slimCounts))]
```

Mean center values by gene.
```{r}
norm.slimCounts = matrix(NA, ncol=ncol(slimCounts), nrow=nrow(slimCounts), 
												 dimnames = list(gene=row.names(slimCounts),
												 								sample=colnames(slimCounts))
												 )
geneMeans = rowMeans(slimCounts)
for (i in 1:ncol(slimCounts)){
	norm.slimCounts[,i] = slimCounts[,i] - geneMeans
}
```

### Clustering

Generate a distance matrix.
```{r}
slimDist = dist(norm.slimCounts)
```

Hierarchically cluster the genes.
```{r}
slimHc = hclust(slimDist)
plot(slimHc)
```


Define clusters.
```{r}
# hist(log10(slimHc$height))
h = median(slimHc$height)
mycl <- cutree(slimHc, h=5)
clusters = split(deGenes, f=mycl)
```

How many genes are in each cluster?
```{r}
tt = table(table(mycl))
tt
```
There are `r tt["1"]` clusters that have 1 gene. The largest cluster has `r names(tt)[length(tt)]` genes.


Drop the cluters that only have 1 gene.
```{r}
has2orMore = which(sapply(clusters, length) > 1)
clusters = clusters[has2orMore]
```

Sort clusters by size, rename in this order.
```{r}
clusters = clusters[order(sapply(clusters, length), decreasing = T)]
names(clusters) = 1:length(clusters)
```

Cluster `r names(clusters)[1]` is the largest with `r length(clusters[[1]])` genes.
Cluster `r names(clusters)[length(clusters)]` has `r length(clusters[[length(clusters)]])` genes.

### Divide big clusters

Break up the larger clusters into smaller clusters.
```{r}
clusters = bustClust(clusters, data=norm.slimCounts , breakSize = 20, verbose=F)
```

How many clusters are there now? (of each size)
```{r}
tt2 = table(sapply(clusters, length))
tt2
```
There are `r tt2["1"]` clusters that have 1 gene. The largest cluster has `r names(tt2)[length(tt2)]` genes.

Drop the cluters that only have 1 gene.
```{r}
has2orMore = which(sapply(clusters, length) > 1)
clusters = clusters[has2orMore]
```

Sort clusters by their names, so the clusters decended from cluster "1" are grouped togther.
```{r}
clusters = clusters[order(names(clusters))]
df = melt(clusters)
names(df) = c("gene", "cluster")
```


### Cluster summary table

For each cluster, note:             

 * how many genes are in the cluster
 * how many of those genes are DE in each time point

```{r}
numberGenes = sapply(clusters, length)

# add info on DE genes
askDE = merge(df, deTable, by.x="gene", by.y=0) #for deTable use row.names

# split by cluster
ADE = split(askDE, f=askDE$cluster)

# count overlap with DE genes per time point
DEat2hours = sapply(ADE, function(f){
	sum(f$fdr.at2hours <= FDR, na.rm = T)
})
DEat4hours = sapply(ADE, function(f){
	sum(f$fdr.at4hours <= FDR, na.rm = T)
})
DEat8hours = sapply(ADE, function(f){
	sum(f$fdr.at8hours <= FDR, na.rm = T)
})

# compile summary
sumry = data.frame(cluster=names(clusters),
								 numberGenes=numberGenes,
								 # 2 hours
								 numDEat2hours = DEat2hours,
								 perDEat2hours = DEat2hours/numberGenes * 100,
								 # 4 hours
								 numDEat4hours = DEat4hours,
								 perDEat4hours = DEat4hours/numberGenes * 100,
								 # 8 hours
								 numDEat8hours = DEat8hours,
								 perDEat8hours = DEat8hours/numberGenes * 100)
```


# Evaluate clusters

## Silhouette 

The [silhouette](https://en.wikipedia.org/wiki/Silhouette_(clustering)) is a popular metric for evaluating clusters. An element that is very close to the other elements within its cluster, and very far from the next nearest cluster will have a silhouette close to 1.  If the elements in this cluster are on average as far away as the elements in the next cluster, the silhouette will be near 0.  The silhoutte can be as low as -1, in a case where an element is actually closer (on average) to the individual elements in the neighboring cluster than it is to the elements in its own cluster.  

In short:

 * silhouette = 1 --> great clustering!
 * silhouette = 0 --> this cluster was defined on a whim.
 * silhouette = -1 --> clustering differently would be better.
 
This value is defined for *each* element in the cluster.  A very good cluster may have many elements with scores near 1 while some individual elements have much lower values.

For each cluster, note:           

 * the mean silhoutte value

To start, use the silhouette method from the cluster package.

```{r}
clusteredGenes = unique(unlist(clusters))
dmatrix = as.matrix(slimDist)
dmatrix = dmatrix[clusteredGenes, clusteredGenes]
clustNames = levels(as.factor(df$cluster))
x=as.integer(as.factor(df$cluster))
names(x) = df$gene
x = x[row.names(dmatrix)]
sil = silhouette(x=x, dmatrix=dmatrix)

fname = paste0("results/", base, "_Sil.pdf")
pdf(fname, height = 15)
plot(sil)
dev.off()

sildf=as.data.frame(sil[,])
```
Saved image to `r fname`.

DIY Silhouette index.
```{r}
fullSil = calcSil(clusters=clusters, dmatrix=dmatrix)
silTable = dcast(fullSil, gene~cluster, value.var = "sil")
```

## based on motif

For each cluster, note:             

 * how many of those genes have an instance of the motif in the promoter?
 
```{r}
# overlap with motif occurrences
moFile = "../PeaksNearDEGenes/results/MA0571.1_FIMO_all_CountPerGeneRegion.txt"
readLines(moFile, n=3)
mo = read.table(moFile, header=T)
askDE = merge(askDE, mo, by="gene", all.x=T)
# the mo table only has genes that had at least one instance of the motif.
# A gene that was DE, but did not have motif instances in any region will
# have an NA value in this table, and it really means 0.
shouldBe0 = is.na(askDE$promoter)
askDE$promoter[shouldBe0] = 0

# split by cluster
ADE = split(askDE, f=askDE$cluster)

# count overlap with motif occurrences
hasMotif = sapply(ADE, function(f){
	sum(f$promoter > 0, na.rm = T)
})

# add to summary
sumry$numHasMotif = hasMotif
sumry$perHasMotif = hasMotif/numberGenes * 100
```
 
How many genes (whole genome) have an instance of the motif? (based on the score limitation given above)
```{r}
total = 33603 #Our gene annotations include this many genes #sum(!is.na(mo$promoter))
hasMo = sum(mo$promoter > 0, ra.rm=T)
percentWithMo = hasMo/total * 100
percentWithMo
```
Of all genes, about `r percentWithMo`% have the motif.
 
 Are any clusters particularly touched by the motif?
```{r}
moClusts = pickClustersWithMotif(sumry, minNumHasMotif=2, 
																 minPerHasMotif=percentWithMo*3, 
																 nullPerWithMotif=percentWithMo)
```

# Results

## Plot RPKM by cluster

For each cluster, plot the RPKM values over time.
```{r}
fname = paste0("results/", base, "_ClusterPlots.pdf")
pdf(fname, width=8, height = 14)
# maybe set height and the number of rows to plot dynamically based on max number of genes in cluster

for (c in names(clusters)){
	cl = clusters[[c]]
	par(mfrow=c(5,4), oma=c(1,1,5,1))
	
	# silhouette summary plot
	boxplot(silTable[,c], las=1, col="violet", main="silhouette index", ylim=c(0,1))
	abline(h=0, col="gray")
	
	# whole plot header
	title(paste("Cluster id:", c, ";", length(cl), "genes"), outer = T)
	if (c %in% moClusts){
		topText = paste0(round(sumry[c,"perHasMotif"],2), "% of genes have motif")
		title(topText, outer=T, line=1)
	}
	
	# plot each gene
	for (i in 1:length(cl)){
		DrawGeneExprLines(cl[i], preReadFpkm = rpkm)
		mots = askDE[askDE$gene==cl[i], "promoter"]
		mtext(paste(mots, "motif occurrences"), side=3, line=0)
	}
}

dev.off()
```

This collection of plots was saved to `r fname`.

## Save clusters

Save cluster membership info.
```{r}
fname = paste0("results/", base, "_ClusterMembers.txt")
write.table(df, fname, row.names = F, col.names = T, sep="\t", quote = F)
```

Saved `r length(unique(df$cluster))` clusters comprising `r length(unique(df$gene))` genes to `r fname`.

Save Cluster Summary.
```{r}
sumryFname = paste0("results/", base, "_ClusterSummary.txt")
write.table(sumry, file=sumryFname, sep="\t", col.names = T, row.names = F, quote = F)
```
The summary table for these clusters is in this file: `r sumryFname`










# match these clusters to DE genes clusters

Read in the cluster membership for the DE genes.


Merge DE cluster membership to sortable cluster membership.


Tally the 'votes' from each DE cluster for corresponding sortable gene cluster.


Make table of De cluster, sortable cluster and linkID.



# subtract DE genes from these clusters --- this might get moved

For sortable clusters in the linkID table, remove De genes and save the notDE cluster.
Update and save the linkID table
Session info.
```{r}
sessionInfo()
```

