---
title: "Difference Clusters"
author: "I. Blakley"
output:
  html_document:
    toc: true
    code_folding: show
    toc_float: true
---

```{r setup, echo=FALSE}
library(knitr)
library(reshape2)
library(cluster)
suppressPackageStartupMessages(library(plotly))
library(RColorBrewer)
knitr::opts_chunk$set(fig.path = 'figures/', fig.show="hold")
knitr::knit_hooks$set(inline = function(x) {
	if (is.numeric(x)){
		x = prettyNum(x, big.mark=",", digits=2)
	}
	paste0("**", x, "**") # so generated values are always bold
})
source("../src/ProjectWide.R")
source("../src/DrawPlots.R")
source("src/Clusters.R")
base="HierDifference"
```

# Introduction

I want groups of genes that are similar in terms of how and when they are affected by ANT.  To make these groups, I use hierarchical clustering with the logFC values from each time point.

## Questions

 * If I use hierarchical clustering based on the affect of ANT (the logFC), what is a good threshold? 
 * If I use an algorithm that needs to know how many clusters to make, what number should I use? 
 * At some candidate threshold, what does my biggest cluster look like? 
 * At some candidate threshold, what does my tightest custer look like? 

The main goal is to produce a file indicating which cluster a gene belongs to so that information can be used by in downstream analysis.  A related goal is to produce enough description of the clusters to judge if they are appropriate for a particular downstream analysis.

Files produced by this markdown will have "*`r base`*" in the name.

# Create clusters

I only want to use genes that do change in at least one time point.  I am interested in grouping genes by how they are affected by ANT, so it doesn't make sense to use genes that are not detectably affected by ANT.

## Input data

Get DE genes list.
```{r}
FDR = getFDR()
allFDR = rep(FDR, 3)
names(allFDR) = c("at2hours", "at4hours", "at8hours")
deTable = getDEtable(comparison = c("at2hours", "at4hours", "at8hours"),
										 unionIntersect = "union",
										 maxFDR = allFDR)
```

The genes AT5G09463, AT5G09462, AT5G09460, and AT5G09461 all occupy the same range in the genome, so they count the exact same reads.  So these four genes will look like a particulary tight cluster.  Cases like this falsely inflate the Silhouette index and other metrics of good clusterings.  Remove any cases where all measurements of the gene, and the first part of the name are all duplicated.
```{r}
determienDuplicatesBy = paste(apply(deTable, 1, paste, collapse="_"),
															substr(row.names(deTable), 1, 7))
isDup = duplicated(determienDuplicatesBy)
dupGenes = row.names(deTable)[isDup]
deTable = deTable[!isDup,]
```
These `r length(dupGenes)` genes were removed because they have identical data (and a nearly identical name) to another gene: `r paste(dupGenes, collapse=", ")`.

```{r}
deGenes = row.names(deTable)
```

Using an FDR of `r FDR`, we considered `r length(deGenes)` genes to be DE in at least one time point.

In comparisons where the gene was not considered DE, set the logFC to 0.
```{r}
# a gene with any NA values is completely NA in the distance matrix, so we're using 0.
deTable[deTable$fdr.at2hours > FDR, "logFC.at2hours"] = 0
deTable[deTable$fdr.at4hours > FDR, "logFC.at4hours"] = 0
deTable[deTable$fdr.at8hours > FDR, "logFC.at8hours"] = 0
```

Get the log fold change values.
```{r}
diffs = deTable[grep("logFC", names(deTable))]
colnames(diffs) = gsub("logFC.","",colnames(diffs))
rpkm = getRPKM(ave=T, sd=F) #for reference later, not for clustering
```

## Normalization

No normalization is needed.  Each of my input values are on the same scale.  They are all in the same ratio space and already on a log scale.

## Clustering

Generate a distance matrix.
```{r}
slimDist = dist(diffs, method = "euclidean")
```

Hierarchically cluster the genes.
```{r}
slimHc = hclust(slimDist, method = "single")
```

Plot a dentrogram of the hierarchy.  Sometimes looking at the dendrogram can present logical thresholds for defining clusters.
```{r fig.show="hold"}
plot(slimHc)
cutHere = 0.15 
bound = c(.01, .2) # the boundry of what I think looks reasonable.

# save dendrogram
fname=paste0("results/", base, "_fullDendrogram.pdf")
pdf(fname, width = 100)
plot(slimHc)
abline(h=cutHere, col="red")
dev.off()
```

This dendrogram is too dense to interpret here. It was saved as an extra-wide pdf (see `r fname`) so it can be viewed later in a searchable format.  That will be nice if you want to look up some particular gene, but it doesn't help us pick a threshold right now.

Another way we might choose a threshold is by looking for the 'elbow' in the height values.  The height represents how far apart two cluster were before they were merged. The process of hierarchical clustering starts by merging things that are closest together and eventually merges everything--even things that were far apart.  If we plot the heights in order, we may see a logical point where we can say that below this is "close", and things that are less than this far apart are so close that they should be merged, but things separated by a greater distance than this are "far", and should not be merged.

```{r echo=FALSE}
# toPlot = data.frame(x=c(1:length(slimHc$height)), y=log(slimHc$height))
# toPlot = toPlot[slimHc$height != 0,]
# plot(toPlot, las=1, xlab="order", ylab="height")
# # trend = lm(data=toPlot, formula = y ~ x, subset=100:500)
# # toPlot$trend = predict.lm(trend, newdata=toPlot)
# # abline(trend, col="red", lty=2)
# # w = with(toPlot, {max(which( y <= trend ))  })
# # segments(x0=w, y0=toPlot$y[w], y1=par("usr")[3], col="red")
# w = 100
# segments(x0=w, y0=toPlot$y[w], y1=par("usr")[3], col="red")
# 
# #Eyeballing it, it looks like the `r w`th joining was the last in the really close
```


```{r}
plot(slimHc$height, 1:length(slimHc$height), las=1, ylab="order")
abline(v=cutHere, col="red"); abline(v=bound, col="purple", lty=2)
mtext(text=cutHere, side=1, line=0, at=cutHere, col="red")
#
plot(slimHc$height, 1:length(slimHc$height), las=1, ylab="order", xlim=c(0,1))
abline(v=cutHere, col="red"); abline(v=bound, col="purple", lty=2)
mtext(text=cutHere, side=1, line=0, at=cutHere, col="red")
#
plot(slimHc$height, 1:length(slimHc$height), las=1, ylab="order", xlim=c(0,.1))
abline(v=cutHere, col="red"); abline(v=bound, col="purple", lty=2)
mtext(text=cutHere, side=1, line=0, at=cutHere, col="red")
```

I think somewhere around 0.01 looks reasonable. I've drawn the boundries of what I think could possibly be considered reasonable in purple dashed lines.


```{r echo=FALSE}
# what is the difference from each height value to the next
# heightDiff = slimHc$height[-1] - slimHc$height[-length(slimHc$height)]
# plot(1:(length(slimHc$height)-1), heightDiff)
# plot(1:(length(slimHc$height)-1), heightDiff, ylim=c(0,.1))
```


Consider the range of values that could be reasonable cuttoffs, the reange from `r bound[1]` to `r bound[2]`.  For each possible cuttoff, try it and see how many clusters that makes and what the mean silhouette value is for all points. Silhouette is described in greater detail in the Silhouette section; in short: higher is better.  Clusters of exactly one element are hardly clusters.  Additionally note the number of clusters and the mean Silhouette if singletons are excluded (show in blue).
```{r echo=FALSE}
candCut = seq(bound[1], bound[2], length.out = 50)
silRes = data.frame(candCut = candCut,
										numClusts = NA,
										meanSilhouette = NA,
										numClustsNoSingles = NA,
										MeanSilhouetteNoSingles = NA)
for (i in 1:length(candCut)){
	cutoff = candCut[i]
	ct = cutree(slimHc, h=cutoff)
	clusterSets = split(names(ct), f=ct, drop=T)
	# count clusters
	silRes[i,"numClusts"] = length(clusterSets)
	# get mean silhouette
	dmatrix = as.matrix(slimDist)[names(ct), names(ct)]
	sil = silhouette(x=ct, dmatrix=dmatrix)
	silRes[i,"meanSilhouette"] = mean(sil[,3])
	# limit to clusters with min size
	clusterSets = clusterSets[sapply(clusterSets, length) > 1]
	ct = ct[unlist(clusterSets)]
	# count clusters
	silRes[i,"numClustsNoSingles"] = length(clusterSets)
	# get mean silhouette
	dmatrix = as.matrix(slimDist)[names(ct), names(ct)]
	sil = silhouette(x=ct, dmatrix=dmatrix)
	silRes[i,"MeanSilhouetteNoSingles"] = mean(sil[,3])
}
```

```{r height=10, echo=F}
par(mfrow = c(2,1), las=1, mar=c(1,4,0,2), oma=c(4,0,2,0))
with(silRes,{
	# upper plot
	plot(candCut, numClusts,
			 type="l", ylab="number of clusters", xaxt="n", ylim=c(0,length(deGenes)/2))
	lines(candCut, numClustsNoSingles, col="blue")
	abline(v=cutHere, col="red")
	legend(x="topright", legend=c("all", "exclude singletons"), col=c("black", "blue"), lty=1)

	# lower plot
	plot(candCut, MeanSilhouetteNoSingles,
			 type="l", ylab="Silhouette", col="blue", ylim=c(0,1))
	lines(candCut, meanSilhouette)
	abline(v=cutHere, col="red")
	mtext(text=cutHere, side=1, line=0, at=cutHere, col="red")
	mtext("height cuttoff to define hierarchical clusters", side=1, line=3)
	title("Choose optimal height cuttoff", outer=T)
})
```

From eye-balling the "elbow", I thought a cuttoff of about 0.1 would be good.  Looking at the results near that, it looks like that range is still reasonable and 0.01 is about as good as any other option near there.  So I'll move forward with a cutoff of `r cutHere`.

## Define tight clusters

Define clsuters.
```{r}
mycl = cutree(slimHc, h=cutHere)
# list of genes per cluster
Tclusters = split(diffs, f=mycl, drop=T)
```

Drop any that have only 3 genes or less. 
```{r}
minGenesInCluster = 4
Tclusters = Tclusters[sapply(Tclusters, nrow) >= minGenesInCluster]
genesInClusters = unlist(sapply(Tclusters, row.names))
```
Removed clusters with less than `r minGenesInCluster` genes.

We had to drop `r length(setdiff(row.names(diffs), genesInClusters))` genes because they didn't cluster.  

Name these clusters "tight_N", where 'N' means it is the Nth biggest clusters.
```{r}
# sort
Tclusters = Tclusters[order(sapply(Tclusters, nrow), decreasing = T)]
# rename
names(Tclusters) = paste0("tight_", 1:length(Tclusters))
```


## Define loose clusters

Do clustering again with only those.
```{r}
lonelyGenes = setdiff(row.names(diffs), genesInClusters)
slimDist2 = dist(diffs[lonelyGenes,], method = "euclidean")
slimHc2 = hclust(slimDist2, method = "single")
cutHere2 = 0.3 
```

Again, we have to pick some cutoff, but we know it will have to be bigger this time.  

```{r echo=FALSE}
bound = c(.1, .5)
#
plot(slimHc2$height, 1:length(slimHc2$height), las=1, ylab="order")
abline(v=cutHere2, col="red"); abline(v=bound, col="purple", lty=2)
mtext(text=cutHere2, side=1, line=0, at=cutHere2, col="red")
#
plot(slimHc2$height, 1:length(slimHc2$height), las=1, ylab="order", xlim=c(0,1))
abline(v=cutHere2, col="red"); abline(v=bound, col="purple", lty=2)
mtext(text=cutHere2, side=1, line=0, at=cutHere2, col="red")
```
Notice that there *are* joins with heights below our original cutoff (`r cutHere`).  We removed all of the clustered genes, but we still have genes that formed clusters that were too small (less than `r minGenesInCluster` genes).  Those tiny clusters could include genes that were very close together.

```{r}
mycl2 = cutree(slimHc2, h=cutHere2)
Lclusters = split(diffs[names(mycl2),], f=mycl2, drop=T)
# sort
Lclusters = Lclusters[order(sapply(Lclusters, nrow), decreasing = T)]
# rename
names(Lclusters) = paste0("loose", 1:length(Lclusters))
#mycl[names(mycl2)] = mycl2

# list of genes per cluster
clusters = c(Tclusters, Lclusters)
```

Drop any that have only 3 genes or less. 
```{r}
clusters = clusters[sapply(clusters, nrow) >= minGenesInCluster]
genesInClusters = unlist(sapply(clusters, row.names))
lonelyGenes = setdiff(row.names(diffs), genesInClusters)
```

We had to drop `r length(lonelyGenes)` genes because they didn't cluster.  

## Put lonely genes into nearest cluster

For each un-clustered gene, determine the nearest clustered gene, and add it to that cluster.
```{r}
# remake mycl using the new clusters names and members
mycl3df = melt(sapply(clusters, row.names))
mycl3 = mycl3df[,2]
names(mycl3) = mycl3df[,1]
# subset the distance matrix, with only rows for in-cluster genes and columns for lonely genes
sD = as.matrix(slimDist)[genesInClusters, lonelyGenes]
# split rows by cluster
splitSD = split(as.data.frame(sD), f=mycl3[genesInClusters])
# For each lonley gene, get the minimum gene distance in each cluster,
# that metric for the distance to the cluster corresponds to the "single" method we used above in clustering.
splitSD = lapply(splitSD, FUN=function(x){
	return(apply(x, MARGIN=2, FUN=min))
}
)
sD = do.call(rbind, splitSD)
for(lone in lonelyGenes){
	w = which(sD[,lone] == min(sD[,lone]))[1] # [1] just in case of equidistant clusters
	targetCluster = row.names(sD)[w]
	mycl[lone] = targetCluster
	clusters[[targetCluster]] = rbind(clusters[[targetCluster]], 
																		diffs[lone, ])
}
# # update the clusters object
# clusters = split(diffs, f=mycl, drop=F) # drop=F because now we expect all genes to belong to a cluster
```


## Finalize clusters

```{r}
clustSizes = sapply(clusters, nrow)
```

That gives me `r length(clustSizes)` clusters, with `r min(clustSizes)` to `r max(clustSizes)` genes in each one.  For more detail, see the table below.

```{r echo=F}
clustSizes
```
Reading this table, I see that cluster `r names(clustSizes)[1]` has `r clustSizes[1]` genes, and cluster `r names(clustSizes)[2]` has `r clustSizes[2]` genes, and so on.

Make a data frame of cluster memberships.
```{r}
df = melt(sapply(clusters, row.names))
names(df) = c("gene", "cluster")
#df$gene = as.character(df$gene)
row.names(df) = df$gene
```

For each gene, indicate if it was clustered in the tight phase, the loose phase or the place-lonely-genes phase.
```{r}
df$clusteringPhase = NA
df[unlist(sapply(Tclusters, row.names)), "clusteringPhase"] = "tight"
df[unlist(sapply(Lclusters, row.names)), "clusteringPhase"] = "loose"
df[lonelyGenes, "clusteringPhase"] = "lonely"
```



## Centroids

Centroids represent the average of each dimension of each cluster.  Using the centroids as the input to hierarchical clustering will give us the relationships between clusters. This is essentally the same dentrogram that we had to begin with, but we have filtered out lonely genes that didn't cluster to nearlby neighbors and we aren't plotting down to the individual genes level, just to the level where we defined these clusters, which is why nothing is shown below `r cutHere`.
```{r}
#centroids = do.call(rbind, lapply(clusters, colMeans))
# because these clusters incluse some far-cry out-liers, use the median rather than the mean
centroids = do.call(rbind, lapply(clusters, apply, 2, median))
# cluster the centroids
centDist = dist(centroids, method = "euclidean")
centHc = hclust(centDist, method = "average")
plot(centHc, las=1)
```

In this dendrogram, the leaves of the tree are not individual genes, but centroids: the points that represent the center of each cluster.   

This figure is repeated in the *Results* section with more extensive manual notes. What we need from it here is just the order.  The order of clusters in this tree puts each one (more or less) next to its most similar neighbor.  That will be a helpful way to look at things later.  

Put the clusters in that order.
```{r}
TheOrder = row.names(centroids)[centHc$order]
clusters = clusters[TheOrder]
centroids = centroids[TheOrder,]
clustSizes = clustSizes[TheOrder]
```


## Cluster summary table

For each cluster, note:           

 * how many genes are in the cluster
 * the centroid values for each dimension

```{r echo=FALSE}
# Originally, I thought it made sense to note the number of DE genes at each time point, and the percentage of DE genes at each time point. But then that doesn't make sense, see note below chunk.

# # add info on DE genes
# askDE = merge(df, deTable, by.x="gene", by.y=0, all.y=F, all.x=T) #for deTable use row.names

# # split by cluster
# ADE = split(askDE, f=askDE$cluster)
# ADE = ADE[names(clustSizes)] #split does not preserve any particular order
# 
# # count overlap with DE genes per time point
# DEat2hours = sapply(ADE, function(f){
# 	sum(f$fdr.at2hours <= FDR, na.rm = T)
# })
# DEat4hours = sapply(ADE, function(f){
# 	sum(f$fdr.at4hours <= FDR, na.rm = T)
# })
# DEat8hours = sapply(ADE, function(f){
# 	sum(f$fdr.at8hours <= FDR, na.rm = T)
# })

# compile summary
# sumry = data.frame(cluster=names(clusters),
# 								 numberGenes=clustSizes,
# 								 # # 2 hours
# 								 # numDEat2hours = DEat2hours,
# 								 # perDEat2hours = round(DEat2hours/clustSizes * 100),
# 								 # # 4 hours
# 								 # numDEat4hours = DEat4hours,
# 								 # perDEat4hours = round(DEat4hours/clustSizes * 100),
# 								 # # 8 hours
# 								 # numDEat8hours = DEat8hours,
# 								 # perDEat8hours = round(DEat8hours/clustSizes * 100),
# 								 # centroids
# 								 centroid.at2hours = centroids[,"at2hours"],
# 								 centroid.at4hours = centroids[,"at4hours"],
# 								 centroid.at8hours = centroids[,"at8hours"])
```
```{r}
sumry = data.frame(cluster=names(clusters),
								 numberGenes=clustSizes,
								 centroid.at2hours = centroids[,"at2hours"],
								 centroid.at4hours = centroids[,"at4hours"],
								 centroid.at8hours = centroids[,"at8hours"])
```

Note that we did not include the number of DE genes per time point.  Since they were clustered by difference, the number of DE genes per time point was always all-or-none.  The percentage was always 0-or-100.  And this information is also related in the centroids--when none were DE, the centroid value is 0; if all are DE the centroid value is not 0.

# Evaluate clusters

## Silhouette

The [silhouette](https://en.wikipedia.org/wiki/Silhouette_(clustering)) is a popular metric for evaluating clusters. An element that is very close to the other elements within its cluster, and very far from the next nearest cluster will have a silhouette close to 1.  If the elements in this cluster are on average as far away as the elements in the next cluster, the silhouette will be near 0.  The silhoutte can be as low as -1, in a case where an element is actually closer (on average) to the individual elements in the neighboring cluster than it is to the elements in its own cluster.  

In short:

 * silhouette = 1 --> great clustering!
 * silhouette = 0 --> this cluster was defined on a whim.
 * silhouette = -1 --> clustering differently would be better.
 
This value is defined for *each* element in the cluster.  A very good cluster may have many elements with scores near 1 while some individual elements have much lower values.

For each cluster, note:           

 * the mean silhoutte value

To start, use the silhouette method from the cluster package.

```{r}
dmatrix = as.matrix(slimDist)[df$gene, df$gene]
sil = silhouette(x=as.numeric(as.factor(df$cluster)), dmatrix=dmatrix)
sildf=as.data.frame(sil[,])
```
```{r echo=F}
fname = paste0("results/", base, "_Sil.pdf")
pdf(fname, height = 15)
plot(sil)
dev.off()
```

For a visualization of the Silhouette values in each cluster, see `r fname`.  Unfortunately, this output assigns arbitrary numbers as the cluster ids, and I haven't found a way to link those back to the original clusters.  There's a value for each gene, but the names and the order are not preserved.  Surely there are other functions for calculating the Silhouette of clusters, but this one particular one dominates my search results.

Since that output cannot be added to my summary table, I have a DIY Silhouette function.  It doesn't make the nice graphic, but it does produce values in a format that can be added to my other output.
```{r}
fullSil = calcSil(clustersDF=df, dmatrix=as.matrix(slimDist)[df$gene, df$gene])
# summaryize the output per-cluster
silTable = dcast(fullSil, gene~cluster, value.var = "sil")
row.names(silTable) = silTable$gene
silTable = silTable[,names(silTable) != "gene"]
```
```{r echo=FALSE}
# fullSil$numberInCluster = clustSizes[fullSil$cluster]
# plot(fullSil$numberInCluster, fullSil$sil, las=1, ylim=c(-1,1), col=fullSil$cluster)
```

Add the Silhouette values to the summary table.
```{r}
silMeans = apply(silTable, 2, mean, na.rm=T)
sumry$meanSilhouette = silMeans[row.names(sumry)]
```


## based on motif

For each cluster, note:           

 * how many of those genes have an instance of the motif in the promoter?
 
```{r}
# overlap with motif occurrences
moFile = "../PeaksNearDEGenes/results/MA0571.1_FIMO_all_CountPerGeneRegion.txt"
readLines(moFile, n=3)
mo = read.table(moFile, header=T)
moDF = merge(df, mo, by="gene", all.x=T)
# the mo table only has genes that had at least one instance of the motif.
# A gene that was DE, but did not have motif instances in any region will
# have an NA value in this table, and it really means 0.
shouldBe0 = is.na(moDF$promoter)
moDF$promoter[shouldBe0] = 0

# split by cluster
HasMO = split(moDF, f=moDF$cluster)

# count overlap with motif occurrences
hasMotif = sapply(HasMO, function(f){
	sum(f$promoter > 0, na.rm = T)
})
hasMotif = hasMotif[row.names(sumry)] # ensure correct order

# add to summary
sumry$numHasMotif = hasMotif
sumry$perHasMotif = round(hasMotif/clustSizes * 100)
```
 
How many genes (whole genome) have an instance of the motif using the above criteria?
```{r}
total = sum(!is.na(mo$promoter))
hasMo = sum(mo$promoter > 0, ra.rm=T)
percentWithMo = hasMo/total * 100
percentWithMo
```
Of all genes, about `r percentWithMo`% have the motif.
 
 Are any clusters particularly touched by the motif?
```{r}
# see src/MotifsInClusters.R
moClusts = pickClustersWithMotif(sumry, minNumHasMotif=2, 
																 minPerHasMotif=percentWithMo, 
																 nullPerWithMotif=percentWithMo)
```

In the plot above, the green dashed line is the percentage of genes with the ANT motif across the genome.  If cluster membership has nothing to do with the motif, then we would expect a randome distribution around that value.  Any clusters that have a much higher percentage of genes with the motif might represent how and when genes with that motif are regulated by ANT.  The gray lines are guides for where a cluster could be plotted if exactly one gene has the motif, or exactly 2, or 3 etc.

# Results

## Plot clusters

For each cluster:                      

 * show a boxplot of the silhouette values for genes in that cluster.
 * show a boxplot representing the range of the logFC value at each timepoint.
 * plot the RPKM values over time for each gene, 
 * For clusters that have a noteworthy percentage of genes with the motif, indicate that at the top.
 
For each gene, plot the RPKM over time. In each plot:     

 * indicate the time points when the change was significant with a red star,
 * indicate which genes have an instance of the motif in the promoter
 * use the same coloring scheme as the rest of the project, green for control and purple for treatment.

This is a lot of visual information.  Start a new page in the pdf for each cluster.
At the beginning of the pdf, show the boxplots for all clusters.
```{r}
fnamePlots = paste0("results/", base, "_ClusterPlots.pdf")
pdf(fnamePlots, width=8, height = 14)
# maybe set height and the number of rows to plot dynamically based on max number of genes in cluster

# Document header
printText = c("This file was generated by:", 
							knitr::current_input(), "",
							"It was generated on:", 
							date(), "",
							"Number of clusters:",
							length(clusters))
textplot(printText)

# cluster centroid dendrogram
par(mfrow=c(3,1), oma=c(4,2,4,2)) # based on pdf dimensions
plot.new() # just to make it fit in the middle of the page
plot(centHc, las=1)

# At the beginning of the document, make a cluster summary page
# This information is repeated on the page for each plot
par(mfrow=c(5,4), oma=c(0,1,3,1))
for (c in names(clusters)){
	cl = row.names(clusters[[c]])
	# cluster range per time point
	boxplot(diffs[cl,], las=1, ylab="LogFC", 
					main=paste0(c, " (", length(cl), ")"), 
					col="lightblue", names=gsub("ours","",colnames(diffs)))
	topText = paste0("mean silhouette: ", round(mean(silTable[,c],na.rm=T),3))
	title(topText, line=.5)
}

for (c in names(clusters)){
	cl = row.names(clusters[[c]])
	par(mfrow=c(5,4), oma=c(1,1,5,1))
	
	# silhouette summary plot
	boxplot(silTable[,c], las=1, col="violet", main="silhouette index", ylim=c(0,1))
	abline(h=0, col="gray")
	
	# whole plot header
	title(paste("Cluster id:", c, ";", length(cl), "genes"), outer = T)
	# if this cluster had a high number/percentatge of motif occurences, note that.
	if (c %in% moClusts){
		topText = paste0(round(sumry[c,"perHasMotif"],2), "% of genes have motif")
		title(topText, outer=T, line=1)
	}
	
	# cluster range per time point
	boxplot(diffs[cl,], las=1, main="Cluster Range", ylab="LogFC", 
					col="lightblue", names=gsub("ours","",colnames(diffs)))
	
	# plot each gene
	for (i in 1:length(cl)){
		DrawGeneExprLines(cl[i], preReadFpkm = rpkm, 
											whenDE=c(2,4,8)[deTable[cl[i],grep("fdr", names(deTable))] < FDR], 
											FDR=FDR)
		mots = moDF[moDF$gene==cl[i], "promoter"]
		mtext(paste(mots, "motif occurrences"), side=3, line=0)
	}
}

dev.off()
```

This collection of plots was saved to `r fnamePlots`.

## Save clusters

Save cluster membership info.
```{r}
fname = paste0("results/", base, "_ClusterMembers.txt")
write.table(df, fname, row.names = F, col.names = T, sep="\t", quote = F)
```

Saved `r length(unique(df$cluster))` clusters comprising `r length(unique(df$gene))` genes to `r fname`.

Save Cluster Summary.
```{r}
sumryFname = paste0("results/", base, "_ClusterSummary.txt")
write.table(sumry, file=sumryFname, sep="\t", col.names = T, row.names = F, quote = F)
```
The summary table for these clusters is in this file: `r sumryFname`

It is also shown here:
```{r echo=F}
kable(sumry, row.names = F)
```


## How clusters relate to each other

```{r}
plot(centHc, las=1)
```

In this dendrogram we can see which clusters are closest together.  For example, tight_12 and tight_13 are very close. Looking at the cluster plots (`r fnamePlots`), we that they both include genes that are up-regulated at the 4-hour time point doubleing in expression, and they are not significantly regulated at the 8-hour time point.  They are separated by being up-regulated at the 2-hour time point (tight_12) or not detectably regulated then (tight_13).

## Visualize clusters

Clustering is often done when there are too many features (dimensions) to visually group elements.  In this case, there are only three dimensions, so we *can* plot it to visually check the clustering.

```{r echo=FALSE}
# using the original input data, indicate which cluster each gene was put it
# for the genes that were not in any of the clusters we kept, add cluster "none"
toPlot = merge(diffs, df, by=0)
toPlot$cluster[is.na(toPlot$cluster)] = "none"
row.names(toPlot) = rownames(toPlot$Row.names)

# assign a color to each cluster
colorSet = c(brewer.pal(8, "Set1")[((1:length(clusters)) %% 7)+1], "#AAAAAAFF")
names(colorSet) = c(names(clusters), "none")
# toPlot$color = colorSet[toPlot$cluster]

# assign a pch based on if the gene based on its clustering phase
# syms = c(rep(2, length(clusters)), 1)
# names(syms) = c(names(clusters), "none")
syms = c(1,2,3)
names(syms) = unique(toPlot$clusteringPhase)

# Create 3D plot
p2 <- plot_ly(toPlot, x = ~at2hours, y = ~at4hours, z = ~at8hours, text = ~gene,
							color = ~cluster, colors = ~colorSet, 
							#I am confused about the symbols; why is symbols=1 working better than symbols=syms ?
							symbol = ~clusteringPhase, symbols = 1, 
							name = ~cluster) %>%
     add_markers() %>%
     layout(scene = list(xaxis = list(title = 'at 2 hours'),
                         yaxis = list(title = 'at 4 hours'),
                         zaxis = list(title = 'at 8 hours')))
p2
```

In the plot above, genes that clustered in the tight phase are plotted as solid squares, genes that clustered in the loose phase are plotted as solid circles and the lonely genes that were added to the nearest cluster (however far away that was) are plotted as hollow circles.


# Conclusions

These `r length(clusters)` clusters represent genes that are similar in terms of how and when they are affected by ANT.

## Answer questions

**If I use hierarchical clustering based on the affect of ANT (the logFC), what is a good threshold?**

The cuttoff I used here was `r cutHere`, for the tight phase and `r cutHere2` for the loose phase. 
I think 0.15 and 0.3 worked out to be pretty reasonable but they could stand some refinement.

**If I use an algorithm that needs to know how many clusters to make, what number should I use?**

In manually curated method, I ended up with `r length(clusters)` clusters, and 19 seems prettey reasonable.  

**At some candidate threshold, what does my biggest cluster look like?**

The biggest cluster, "tight_1", has 102 genes, and they are slightly down-regulated at only the 4-hour time point.  When I tried a single-cuttoff hierarchical cluster method, the largest cluster was the one with a slight down-regulation in the 4-hour time point.

**At some candidate threshold, what does my tightest custer look like?**
 
Based on the Silhouette value (or closely related variant), the tightest cluster is also cluster "tight_1".

## To Do

I think I messed up the silhouette values.

____________
 
**Session info.**
```{r echo=FALSE}
sessionInfo()
```

